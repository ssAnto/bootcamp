package com.example.example.utilities;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.example.R;

public class Loading {
    public static ProgressDialog loadingAnimationdanText(Context context, String text){
        ProgressDialog loading = new ProgressDialog(context);
        loading.setProgressStyle(android.R.style.Widget_ProgressBar_Inverse);
        loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false);
        loading.setMessage(text);

        return loading;
    }

    public static ProgressDialog customLoadingAnimation(Context context){
        ProgressDialog loading = new ProgressDialog(context, R.style.CustomLoadingStyle);
        loading.setProgressStyle(android.R.style.Widget_ProgressBar_Inverse);
        loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false);
        loading.setMessage("");
        return loading;
    }
}
