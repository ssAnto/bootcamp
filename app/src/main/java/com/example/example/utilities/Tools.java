package com.example.example.utilities;

import android.content.Context;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class Tools {
    public static void hideKeyboard(Context context, IBinder windowsToken){
        if(windowsToken!=null){
            InputMethodManager inp = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inp.hideSoftInputFromWindow(windowsToken,0);
        }else{
            Toast.makeText(context, "Waw ... Keren ... ", Toast.LENGTH_SHORT).show();
        }

    }
}
