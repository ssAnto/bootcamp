package com.example.example.utilities;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {
    static String SHARED_PREFERENCES_NAME="SESSION_MANAGER_V1";
    static String UNAME="USERNAME";
    static String PASS="PASSWORD";
    static String REMEMBER="REMEMBER";
    static String LOGIN_FLAG="LOGIN_FLAG";
    static String FCM_TOKEN="TOKEN";

    //konstruktor untuk menggunakan shared preferences
    protected static SharedPreferences retrieveSharedPreferences(Context context) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    //untuk CRUD
    protected static SharedPreferences.Editor retrieveSharedPreferencesEditor(Context context)
    {
        return retrieveSharedPreferences(context).edit();
    }

    //procedure sesuai kebutuhan
    public static void simpanDataLogin(Context context, String uname, String pass,Boolean remember){

        SharedPreferences.Editor editor = retrieveSharedPreferencesEditor(context);

        editor.putString(UNAME,uname);
        editor.putString(PASS,pass);
        editor.putBoolean(REMEMBER,remember);
        editor.putBoolean(LOGIN_FLAG,true);

        editor.commit();
    }

    //cek flag login
    public static boolean cekLoginFlag(Context context)
    {
        return retrieveSharedPreferences(context).getBoolean(LOGIN_FLAG,false);
    }

    //ambil uname
    public static String getUnameSessionManager(Context context)
    {
        return retrieveSharedPreferences(context).getString(UNAME,"");
    }
    //ambil password
    public static String getPassSessionManager(Context context)
    {
        return retrieveSharedPreferences(context).getString(PASS,"");
    }
    //cek remember
    public static Boolean cekRemember(Context context)
    {
        return retrieveSharedPreferences(context).getBoolean(REMEMBER,false);
    }
    //logout
    public static void clearData(Context context)
    {
        SharedPreferences.Editor editor = retrieveSharedPreferencesEditor(context);

        if(cekRemember(context)){
            editor.putBoolean(LOGIN_FLAG, false);
        }
        else {
            editor.putString(UNAME,"");
            editor.putString(PASS,"");
            editor.putBoolean(REMEMBER,false);
            editor.putBoolean(LOGIN_FLAG,false);
        }
        editor.commit();
    }

    public static void setFcmToken(Context context,String fcmToken) {
        SharedPreferences.Editor editor = retrieveSharedPreferencesEditor(context);
        editor.putString(FCM_TOKEN,fcmToken);
        editor.commit();
    }

    public static String getFcmToken(Context context){
        return retrieveSharedPreferences(context).getString(FCM_TOKEN,"");
    }
}
