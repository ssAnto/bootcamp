package com.example.example.utilities;

import com.example.example.models.PhotoModel;

public class TemporaryData {
    private static PhotoModel photoModel;

    public static PhotoModel getPhotoModel() {
        return photoModel;
    }

    public static void setPhotoModel(PhotoModel photoModel) {
        TemporaryData.photoModel = photoModel;
    }
}
