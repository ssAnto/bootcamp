package com.example.example.utilities;

public class Konstanta {
    public static long DELAY_PLASH_SCREEN = 3000;
    public static String USERNAME="USERNAME";
    public static String PASSWORD="PASSWORD";
    public static String REMEMBER="REMEMBER";

    public static String[] ARRAY_PEKERJAAN={
            "\n- Pilih Pekerjaan -\n",
            "\nDesigner\n",
            "\nProgramer\n",
            "\nSystem Analyst\n",
            "\nQA & QC\n",
            "\nProject Manager\n",
            "\nOther\n"
    };

    public static String[] ARRAY_HOBY={
            "\n- Pilih Hobi -\n",
            "\nMemancing Keributan\n",
            "\nDemo\n",
            "\nMenghancurkan Perabotan Rumah\n",
            "\nTidur\n",
            "\nMakan\n",
            "\nOther\n"
    };


    public static String[] ARRAY_JURUSAN={
            "-PILIH JURUSAN-",
            "INFORMATIKA",
            "TEKNIK KOMPUTER",
            "HUKUM",
            "PSIKOLOGI",
            "MANAJEMEN BISNIS",
            "ELEKTRONIKA"
    };
}
