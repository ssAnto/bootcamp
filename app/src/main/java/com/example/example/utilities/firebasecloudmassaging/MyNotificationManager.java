package com.example.example.utilities.firebasecloudmassaging;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.example.example.R;

public class MyNotificationManager {
    private Context context;
    private static final int NOTIFICATION_ID = 1234;
    private static final String CHANNEL_ID="NotificationFCM";
    private static final String CHANNEL_NAME="Example223";
    private static final String CHANNEL_DESCRIPTION="Notifikasi ITDP2";

    public MyNotificationManager(Context context) {
        this.context = context;
    }
    public void showNotification(String from, String message, Intent intent) {
        //pendding intent : intent yg akan dieksekusi saat notifikasi di klik
        PendingIntent pendingIntent = PendingIntent.getActivities(
                context,
                NOTIFICATION_ID,
                new Intent[]{intent},
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context,
                CHANNEL_ID
        );

        Notification notification = builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_launcher_round)
                .setAutoCancel(true)
                .setContentText(from)
                .setContentText(message)
                .setLargeIcon(BitmapFactory
                        .decodeResource(context.getResources(),R.drawable.ic_launcher_round))
                .build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(context.NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.setDescription(CHANNEL_DESCRIPTION);
        }
        notificationManager.notify(NOTIFICATION_ID,notification);
    }
}
