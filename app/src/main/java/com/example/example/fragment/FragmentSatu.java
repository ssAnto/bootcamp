package com.example.example.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.example.R;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;

public class FragmentSatu extends Fragment {
    private ExpandableLinearLayout layout1,layout2;
    private TextView account,deskripsi;
    private boolean togle1=true,togle2=false;
    public FragmentSatu() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View customView = inflater.inflate(R.layout.fragment_satu_layout,container,false);
        //init


        account = customView.findViewById(R.id.expand1);
        deskripsi = customView.findViewById(R.id.expand2);
        layout1 = customView.findViewById(R.id.expandableLayout1);
        layout2 = customView.findViewById(R.id.expandableLayout2);

        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(togle1){

                    layout1.expand();
                }else {

                    layout1.collapse();
                }
                togle1=!togle1;
            }
        });
        deskripsi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(togle2){

                    layout2.expand();
                }else {

                    layout2.collapse();
                }
                togle2=!togle2;
            }
        });

        return customView;
    }
}
