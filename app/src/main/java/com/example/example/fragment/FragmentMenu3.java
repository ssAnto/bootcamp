package com.example.example.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import com.example.example.R;
import com.example.example.adapters.TabLayoutInsideFragmentAdapter;
import com.google.android.material.tabs.TabLayout;


public class FragmentMenu3 extends Fragment {

    private TabLayout slidingTabInsideFragment;
    private ViewPager viewPagerinsideFragment;

    public FragmentMenu3() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_menu3, container, false);

        //init
        slidingTabInsideFragment=view.findViewById(R.id.slidingTabInsideFragment);
        viewPagerinsideFragment=view.findViewById(R.id.viewPagerInsideFragment);

        TabLayoutInsideFragmentAdapter adapter = new TabLayoutInsideFragmentAdapter(getChildFragmentManager());

        adapter.addFragment(new FragmentSatu(),"Fragment Satu");
        adapter.addFragment(new FragmentDua(),"Fragment Dua");
        adapter.addFragment(new FragmentTiga(),"Fragment Tiga");

        viewPagerinsideFragment.setAdapter(adapter);
        slidingTabInsideFragment.setupWithViewPager(viewPagerinsideFragment);
        return view;
    }


}
