package com.example.example.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.example.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;
import java.util.List;

public class ImageSliderFragment extends Fragment {
    private ImageView sliderImageItem;
    private String imageURL;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        //init
        final View contentView = inflater.inflate(R.layout.image_slider_fragment,container,false);
        sliderImageItem=contentView.findViewById(R.id.sliderImageItem);

        //set image
        Glide.with(this)
                .load(imageURL)
                //.placeholder(R.drawable.ic_default_image_account)
                //.error(R.drawable.ic_broken)
                .into(sliderImageItem);


        //set action listener
        sliderImageItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //init fresco
                Fresco.initialize(contentView.getContext());

//                //imageviewer
//                ArrayList<String> urlImage = new ArrayList<>();
//                urlImage.add(getImageURL());
//
//                new ImageViewer.Builder(contentView.getContext(),urlImage)
//                        .setStartPosition(0)
//                        .show();

                //image viewer semuanya
                new ImageViewer.Builder(contentView.getContext(),getImageUrls())

                        .setStartPosition(getPosition())
                        .show();
            }
        });
        return contentView;
    }

    private List<String> imageUrls;
    private int position;

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
