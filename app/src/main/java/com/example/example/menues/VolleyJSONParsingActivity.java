package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.example.R;
import com.example.example.adapters.ListUserAdapter;
import com.example.example.models.UserModel;
import com.example.example.utilities.Loading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VolleyJSONParsingActivity extends AppCompatActivity {
    private Context context = this;
    private Button buttonGetListUser;
    private ListView listUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volley_jsonparsing);

        //init
        buttonGetListUser = findViewById(R.id.button_ambil_list_user);
        listUser = findViewById(R.id.listuser);

        buttonGetListUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserlistFromAPI();
            }
        });


    }

    void getUserlistFromAPI(){

    final ProgressDialog loading = Loading.customLoadingAnimation(context);
    String url = "https://reqres.in/api/users?page=2";
    JsonObjectRequest requestJSON = new JsonObjectRequest(
            Request.Method.GET,
            url,
            null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    System.out.println(response.toString());

                    //parsing JSON
                    try {
                        JSONArray jsonArray = response.getJSONArray("data");
                        ArrayList<UserModel> userModels = new ArrayList<UserModel>();
                        if (jsonArray.length() > 0) {
                            //setiap looping ambil data
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                UserModel model = new UserModel();
                                model.setId(jsonObject.getInt("id"));
                                model.setEmail(jsonObject.getString("email"));
                                model.setFirst_name(jsonObject.getString("first_name"));
                                model.setLast_name(jsonObject.getString("last_name"));
                                model.setAvatar(jsonObject.getString("avatar"));

                                userModels.add(model);
                            }
                        } else {
                            Toast.makeText(context, "Data Tidak ada !!!", Toast.LENGTH_LONG).show();
                        }
                        isiListAdapter(userModels);
                    } catch (JSONException e) {
                        System.out.println("Error Parsing JSON " + e.getMessage());
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "Error oi ... " + error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
    );
        Volley.newRequestQueue(context).add(requestJSON);
}
void isiListAdapter(ArrayList<UserModel> userModels){
    ListUserAdapter adapter = new ListUserAdapter(context,userModels);
    listUser.setAdapter(adapter);
}
}
