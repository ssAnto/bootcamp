package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.example.R;
import com.example.example.adapters.CustomListAdapter;

public class ListViewActivity extends AppCompatActivity {
    private Context context = this;
    private String[] MEREK_MOBIL ={
            "Honda",
            "Toyota",
            "Mitsubishi",
            "BMW",
            "Ferrary"
    };
    private String[] DESKRIPSI_MOBIL={
            "Honda adalah motor di padang",
            "Toyota adalah mobil",
            "Mitsubhishi adalah ya mobil goblok !!!",
            "BMW untuk orang Gengsian",
            "Ferrari untuk sultan"
    };
    private int[] ICON_MOBIL = {
        R.drawable.logo_honda,
            R.drawable.toyota,
            R.drawable.mitsubishi,
            R.drawable.bmw,
            R.drawable.ferrari
    };
    private ListView listMerekMobil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        listMerekMobil=findViewById(R.id.merek_mobil);
        //isi list default
        isiListCustom();
    }
    void isiListCustom(){
        CustomListAdapter adapterKostum = new CustomListAdapter(context,
                MEREK_MOBIL,
                DESKRIPSI_MOBIL,
                ICON_MOBIL);
        listMerekMobil.setAdapter(adapterKostum);
    }

    void isiListDefault(){
        ArrayAdapter<String> adapterList = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1,
                MEREK_MOBIL);
        listMerekMobil.setAdapter(adapterList);
    }

}
