package com.example.example.menues;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.example.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.IOException;

public class GaleryActivity extends AppCompatActivity {
    private Context context = this;
    private Button buttonGalery;
    private ImageView reviewPhotofromGalery;

    //request id
    private  int ID_PEMANGGIL = 2;
    private  int REQUEST_PERMISION_FOR_GALERY=22;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galery);

        //init
        buttonGalery = findViewById(R.id.buttonGalery);
        reviewPhotofromGalery = findViewById(R.id.reviewPhotofromGalery);

        buttonGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkPermissionGalery()){
                    ambilImageDariGalery();
                }
            }
        });
    }

    private boolean checkPermissionGalery(){
        boolean result = true;
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISION_FOR_GALERY);
                 result=false;
            }
        }
        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode==REQUEST_PERMISION_FOR_GALERY){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                ambilImageDariGalery();
            }else{
                Toast.makeText(context,"Berikan Akses jika ingin gunakan ini !!!",Toast.LENGTH_LONG).show();
            }
        }
    }

    void ambilImageDariGalery(){
        //untuk ambil gambar dari galeri
        Intent intentGalery = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intentGalery.setType("image/*");
        intentGalery.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intentGalery, "Pilih Photo"),
                ID_PEMANGGIL);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==ID_PEMANGGIL
                && resultCode == Activity.RESULT_OK){
            showImageFromGalery(data);
        }else{
            //gagal

        }
    }

    void showImageFromGalery(Intent data){
        if(data!= null){
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(),
                        data.getData());

                Glide.with(context)
                        .load(bitmap)
                        .into(reviewPhotofromGalery);


            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(context,"Error : "+e.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    }
}
