package com.example.example.menues;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ethanhua.skeleton.RecyclerViewSkeletonScreen;
import com.ethanhua.skeleton.Skeleton;
import com.example.example.R;
import com.example.example.adapters.ListUserRetrofitAdapter;
import com.example.example.models.list_user.Datum;
import com.example.example.models.list_user.ListUserModel;
import com.example.example.retrofit_utilities.APIUtilities;
import com.example.example.retrofit_utilities.RequestAPIServices;
import com.example.example.utilities.Loading;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitActivity extends AppCompatActivity {
    private Context context=this;
    private Button tombol;
    private RecyclerView listUser;
    private SwipeRefreshLayout swipeRefresh;

    private RequestAPIServices apiServices;
    private List<Datum> listUsers = new ArrayList<>();
    private ListUserRetrofitAdapter adapterUser,tmp;
    private RecyclerViewSkeletonScreen skeletonLoading=null;

    //untuk panggil pagging
    private int page = 1; // nilai min
    private int total_page = 1; // nilai max
    private boolean isStillLoading = false;

    @Override
    protected void onPause() {
        super.onPause();
         skeletonLoading.hide();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);

        //init
        tombol=findViewById(R.id.button_retrofit);
        listUser=findViewById(R.id.listUserRetrofit);
        swipeRefresh = findViewById(R.id.swipeRefresh);

        //untuk reset (refresh)
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                Toast.makeText(context,"Sedang Refresh",Toast.LENGTH_LONG).show();

                //untuk set nilai ke awal ....
                resetToDefault();
                //untuk hilangkan tampilan refreshnya ...
                swipeRefresh.setRefreshing(false);
            }
        });

        tombol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                panggilAPIListUser(page);
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
        listUser.setLayoutManager(layoutManager);
    }


    void resetToDefault(){
        page=1;
        total_page=1;
        isStillLoading=false;
        listUsers = new ArrayList<>();
        adapterUser=null;
        panggilAPIListUser(page);
    }

    void tampilkanIsiListUser(){

        //dia akan membuat adapter user sekali saja di awal ...
        if(adapterUser==null){
            adapterUser = new ListUserRetrofitAdapter(context, listUsers);
            listUser.setAdapter(adapterUser);
        }
        tmp=null;
        //selanjutnya recyclser view akan melakukan pencocokan ketia ada data berubah ..
        //method khusus recycle view biar tau ada perubahan data dan di refresh contentnya
        adapterUser.notifyDataSetChanged();

        //paging load more
        loadMorePagging();
    }

    void loadMorePagging(){
        final LinearLayoutManager layoutManager =
                (LinearLayoutManager) listUser.getLayoutManager();

        //deteksi event proses scroll swipe down
        listUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //untuk meneteksi scroll vertical
                boolean isScrollVertical = recyclerView.canScrollVertically(1);

                //deteksi posisi item paling bawah
                int lastItemPosition = layoutManager.findLastVisibleItemPosition();

                //logic
                if(lastItemPosition == layoutManager.getItemCount()-1
                    && !isScrollVertical
                    && page < total_page
                    && !isStillLoading){

                    page++;
                    panggilAPIListUser(page%3);
                }
            }
        });
    }

    void panggilAPIListUser(int page){
        skeletonLoading = new Skeleton()
                .bind(listUser)
                .adapter(tmp)
                .load(R.layout.list_user_skeleton)
                .shimmer(true)
                .duration(500).show();
//        final ProgressDialog loading = Loading.customLoadingAnimation(context);
//        loading.show();
        isStillLoading=true;

        //panggil api retrofit
        apiServices= APIUtilities.getAPIServices();
        apiServices.getListUser(page).enqueue(new Callback<ListUserModel>() {
            @Override
            public void onResponse(Call<ListUserModel> call, Response<ListUserModel> response) {
//                loading.dismiss();
                skeletonLoading.hide();
                isStillLoading=false;

                //handle http response code
                if(response.code()==200){

                    //cek pagenya kosong ndak ...
                    if(listUsers.size()>0){
                        //tambah isi list user yg sebelumnya ...
                        List<Datum> tambah = response.body().getData();
                        for (int i = 0; i < tambah.size(); i++) {
                            listUsers.add(tambah.get(i));
                        }
                    }else{
                        //kondisi mengisi listUser dan total pagenya
                        listUsers = response.body().getData();
                        total_page = response.body().getTotalPages() * 8;
                    }
                    //menampilkan isi listUser
                    tampilkanIsiListUser();
                }else{
                    System.out.println("Sesuatu ada yg salah dgn anda !!!!"+response.code());
                }
            }

            @Override
            public void onFailure(Call<ListUserModel> call, Throwable t) {
                System.out.println("Error : "+t.getMessage());
                skeletonLoading.hide();
//                loading.dismiss();
                isStillLoading=false;
            }
        });
    }
}
