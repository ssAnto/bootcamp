
package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.example.R;
import com.example.example.adapters.ListPhoto;
import com.example.example.models.PhotoModel;
import com.example.example.models.UserModel;
import com.example.example.utilities.Loading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdvanceParsingActivity extends AppCompatActivity {
    private Context context = this;
    private Button load;
    private ListView listImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advance_parsing);

        // init
        load = findViewById(R.id.button_load);
        listImage = findViewById(R.id.list_image);

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jsonParsing();
            }
        });
    }

    void jsonParsing(){

        final ProgressDialog loading = Loading.customLoadingAnimation(context);
        loading.show();

        String url = "https://jsonplaceholder.typicode.com/photos?albumId=1";
        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<PhotoModel> photoModels = new ArrayList<PhotoModel>();
                        if (response.length() > 0) {
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    PhotoModel model = new PhotoModel();
                                    model.setId(jsonObject.getInt("id"));
                                    model.setAlbungId(jsonObject.getInt("albumId"));
                                    model.setThumbnailUrl(jsonObject.getString("thumbnailUrl"));
                                    model.setUrl(jsonObject.getString("url"));
                                    model.setTitle(jsonObject.getString("title"));

                                    photoModels.add(model);
                                } catch (JSONException e) {
                                    System.out.println("Error = " + e.getMessage());
                                }
                            }
                            //isi ke adapter
                            isiListAdapter(photoModels);
                        } else {
                            System.out.println("Data Tidak Ada !!!");
                        }
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("Error : "+error.getMessage());
                        loading.dismiss();
                    }
                }
        );
        Volley.newRequestQueue(context).add(request);
    }
    void isiListAdapter(ArrayList<PhotoModel> photoModels){
        ListPhoto adapter = new ListPhoto(context,photoModels);
        listImage.setAdapter(adapter);
    }

}
