package com.example.example.menues;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.example.R;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DigitalSignatureActivity extends AppCompatActivity {

    private Context context=this;
    private Button save,clear;
    private SignaturePad signaturePad;

    private int PERMISSION_REQUEST_WRITE_EXTERNAL = 29;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital_signature);

        //init
        save=findViewById(R.id.save_button);
        clear=findViewById(R.id.clear_button);
        signaturePad = findViewById(R.id.signaturePad);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap signatureBitmap = signaturePad.getSignatureBitmap();
                saveSignatureToGalery(signatureBitmap);
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
            }
        });

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                Toast.makeText(context, "Start Signing", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                clear.setEnabled(true);
                save.setEnabled(true);
            }

            @Override
            public void onClear() {
                clear.setEnabled(false);
                save.setEnabled(false);
            }
        });
        cekPermision();
    }

    private void saveSignatureToGalery(Bitmap signatureBitmap) {
        //convert bitmap ke ...
        //buat filenya dulu
        File image = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
                String.format("TTD_%d.jpg",System.currentTimeMillis()));
        saveBitmaptoJPG(signatureBitmap,image);
    }

    private void saveBitmaptoJPG(Bitmap signatureBitmap, File file) {
        Bitmap newBitmap = Bitmap.createBitmap(signatureBitmap.getWidth()
                ,signatureBitmap.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas((newBitmap));
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(signatureBitmap,0,0,null);
        try {
            OutputStream outputStream = new FileOutputStream(file);
            newBitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
            outputStream.close();
            Toast.makeText(context, "Berhasil Disimpan", Toast.LENGTH_SHORT).show();
        }catch (FileNotFoundException e){
            System.out.println("Error : "+e.getMessage());
        }catch(IOException d){
            System.out.println("Error : "+d.getMessage());
        }
    }

//    File getStoredgeDirectory(String albumName){
////        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
////                albumName);
////        if(!file.mkdir()){
////            System.out.println("Directory Gagak Dibuat !!!!");
////        }
////        return file;
////        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DCIM),
////                albumName);
////        if(!file.mkdir()){
////            System.out.println("Directory Gagak Dibuat !!!!");
////        }
////        return file;
//        if(!file.mkdir()){
//            System.out.println("Directory Gagak Dibuat !!!!");
//        }
//        return file;
//    }


    private void cekPermision() {
        int currentAPI = Build.VERSION.SDK_INT;
        if(currentAPI >= Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_WRITE_EXTERNAL);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode==PERMISSION_REQUEST_WRITE_EXTERNAL
        && grantResults[0]!=PackageManager.PERMISSION_GRANTED){
            Toast.makeText(context, "ANDA HARUS MEMBERIKAN IJIN !!!!", Toast.LENGTH_SHORT).show();
        }
    }
}
