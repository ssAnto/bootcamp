package com.example.example.menues.perpustakaan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.example.R;
import com.example.example.database_utilities.DatabasePerpustakaanHelper;

import java.io.IOException;

public class PerpustakaanActivity extends AppCompatActivity {
    private Context context = this;

    private boolean cekDb;
    private Button btnimportDatabase,btnlistBuku,btnAddBuku;
    private DatabasePerpustakaanHelper databasePerpustakaanHelper = new DatabasePerpustakaanHelper(context);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perpustakaan);

        //init
        btnimportDatabase=findViewById(R.id.btnimportDatabase);
        btnlistBuku = findViewById(R.id.btnlistBuku);
        btnAddBuku = findViewById(R.id.btnAddBuku);

        btnimportDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                importDatabase();

            }
        });


        btnlistBuku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ListBukuActivity.class);
                startActivity(intent);
            }
        });

        btnAddBuku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,AddBukuActivity.class);
                startActivity(intent);
            }
        });

        importDatabase();
        if(cekDb){
            btnimportDatabase.setEnabled(false);
            btnlistBuku.setEnabled(true);
            btnAddBuku.setEnabled(true);
        }else{
            btnimportDatabase.setEnabled(false);
            btnlistBuku.setEnabled(true);
            btnAddBuku.setEnabled(true);
        }
    }

    void importDatabase() {
        try {
           cekDb = databasePerpustakaanHelper.createDatabaseFromImportedFile();
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("Error : "+e.getMessage());
        }
    }
}
