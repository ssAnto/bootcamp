package com.example.example.menues.mahasiswa;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.example.example.R;
import com.example.example.database_utilities.DatabaseConstanta;
import com.example.example.database_utilities.DatabaseMahasiswaHelper;
import com.example.example.utilities.Konstanta;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EditDataSiswaActivity extends AppCompatActivity {

    private Context context = this;
    private EditText inputNama,inputNomorInduk,inputAlamatMahasiswa;
    private RadioGroup genderMahasiswa;
    private ImageView inputFotoMhs;
    private FloatingActionButton buttonEditDataMhs;
    private Spinner spinnerJurusanMHS;
    private TextView tanggalLahir;
    private DatabaseMahasiswaHelper databaseHelper;
    private static String PHOTO_PATH ="";
    private int id;
    private RadioButton genderL,genderP;
    private String tanggal_tmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_data_siswa);

        //init helpter
        databaseHelper = new DatabaseMahasiswaHelper(context);

        inputNama = findViewById(R.id.inputNama);
        inputNomorInduk = findViewById(R.id.inputNomorInduk);
        inputAlamatMahasiswa = findViewById(R.id.inputAlamatMahasiswa);
        genderMahasiswa = findViewById(R.id.genderMahasiswa);
        inputFotoMhs = findViewById(R.id.editFotoMhs);
        buttonEditDataMhs = findViewById(R.id.buttonSimpanDataMhs);
        tanggalLahir=findViewById(R.id.editTanggal);
        spinnerJurusanMHS = findViewById( R.id.spinnerJurusanMHS);
        genderL = findViewById(R.id.gender_l);
        genderP = findViewById(R.id.gender_p);

        buttonEditDataMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validasiInputDataMahasiswa();
            }
        });

        inputFotoMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ambilFotoProfil();
            }
        });
        tanggalLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTanggalLahir(tanggal_tmp);
            }
        });


        readDataMhs();
    }

    void ambilIdUser(){
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            id=bundle.getInt(DatabaseConstanta.ID);
            System.out.println(id);
        }
        System.out.println(id);
    }

    void readDataMhs(){
        ambilIdUser();
        isiSpinerJurusan();

        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        String queryRead = "SELECT * FROM " + DatabaseConstanta.TABEL_BIODATA+
                " WHERE " +DatabaseConstanta.ID +
                " = " + id;

        System.out.println(queryRead);

        Cursor cursor = db.rawQuery(queryRead,null);

        if(cursor.getCount() == 1){
            cursor.moveToFirst();

            String nim = cursor.getString(1);
            inputNomorInduk.setText(nim);

            String nama = cursor.getString(2);
            inputNama.setText(nama);

            String alamat = cursor.getString(5);
            inputAlamatMahasiswa.setText(alamat);

            String gender = cursor.getString(3);


            if(gender.equalsIgnoreCase("Laki-Laki")){
                genderL.setChecked(true);
            }else {
                genderP.setChecked(true);
            }

            tanggal_tmp = cursor.getString(4);
            tanggalLahir.setText(tanggal_tmp);



            String jurusan = cursor.getString(6);
            int index=0;
            for (int i = 0; i < Konstanta.ARRAY_JURUSAN.length; i++) {
                if(jurusan.equalsIgnoreCase(Konstanta.ARRAY_JURUSAN[i])){
                    index=i;
                }
            }
            spinnerJurusanMHS.setSelection(index);

            //photo
            PHOTO_PATH = cursor.getString(7);
            Glide.with(context).load(PHOTO_PATH).into(inputFotoMhs);
        }
    }


    void isiSpinerJurusan(){
        ArrayAdapter<String> adapterJurusan = new ArrayAdapter<>(
                context,
                android.R.layout.simple_spinner_item,
                Konstanta.ARRAY_JURUSAN
        );
        adapterJurusan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerJurusanMHS.setAdapter(adapterJurusan);
    }

    void  setTanggalLahir(String tanggal){
            final int yearnow = Integer.parseInt(tanggal.substring(6));
            final int monthnow = Integer.parseInt(tanggal.substring(3, 5))-1;
            final int daynow = Integer.parseInt(tanggal.substring(0, 2));
            DatePickerDialog tanggal_lahir = new DatePickerDialog(
                    context,
                    R.style.CustomDatePicker,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                            Calendar selectedDate = Calendar.getInstance();
                            selectedDate.set(year, month, dayOfMonth);


                            //konversi ke string
                            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                            String tanggal = formatter.format(selectedDate.getTime());

                            tanggalLahir.setText(tanggal);
                            tanggal_tmp = tanggal;
                        }
                    },
                    yearnow,
                    monthnow,
                    daynow
            );
            tanggal_lahir.show();
    }

    void validasiInputDataMahasiswa(){
        String nim = inputNomorInduk.getText().toString().trim();
        String nama=inputNama.getText().toString().trim();
        String tanggal_lahir = tanggalLahir.getText().toString().trim();
        String alamat = inputAlamatMahasiswa.getText().toString().trim();

        int selectedGender = genderMahasiswa.getCheckedRadioButtonId();;
        int jurusan = spinnerJurusanMHS.getSelectedItemPosition();

        String jurusanSelected = Konstanta.ARRAY_JURUSAN[jurusan];


        String tmp = "Belum Di Isi !!!!!!!";
        if(nim.length() == 0){
            Toast.makeText(context, "NIM "+tmp, Toast.LENGTH_SHORT).show();
        }else if(nama.length() == 0){
            Toast.makeText(context, "NAMA "+tmp, Toast.LENGTH_SHORT).show();
        }else if(tanggal_lahir.length()==0){
            Toast.makeText(context, "Tanggal Lahir "+tmp, Toast.LENGTH_SHORT).show();
        }else if(alamat.length()==0){
            Toast.makeText(context, "Alamat "+tmp, Toast.LENGTH_SHORT).show();
        }else if(jurusan==0){
            Toast.makeText(context, "Jurusan  "+tmp, Toast.LENGTH_SHORT).show();
        }else if(selectedGender==-1){
            Toast.makeText(context, "Gender "+tmp, Toast.LENGTH_SHORT).show();
        }else if(PHOTO_PATH.length()==0){
            Toast.makeText(context, "Foto Profil "+tmp, Toast.LENGTH_SHORT).show();
        }else{
            //valit ...
            String gender = "Perempuan";
            if( selectedGender == R.id.gender_l){
                gender="Laki-Laki";
            }

            System.out.println(selectedGender);
            System.out.println(gender);

            editDatabaseMahasiswa(
                    nim,
                    nama,
                    tanggal_lahir,
                    alamat,
                    jurusanSelected,
                    gender,
                    PHOTO_PATH
            );

        }
    }

    void editDatabaseMahasiswa(
            String nim,
            String nama,
            String tanggal_lahir,
            String alamat,
            String jurusan,
            String gender,
            String photoPath) {

          // cara raw query ...
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
//
//        String querry_update = "UPDATE  "+ DatabaseConstanta.TABEL_BIODATA+" SET " +
//                "" +DatabaseConstanta.NIM +" = '"+nim+"'"+
//                "," +DatabaseConstanta.NAMA_LENGKAP +" = '"+nama+"'"+
//                "," +DatabaseConstanta.TANGGAL_LAHIR+" = '"+tanggal_lahir+"'"+
//                "," +DatabaseConstanta.ALAMAT+" = '"+alamat+"'"+
//                "," +DatabaseConstanta.JURUSAN+" = '"+jurusan+"'"+
//                "," +DatabaseConstanta.GENDER+" = '"+gender+"'"+
//                "," +DatabaseConstanta.PATH_PHOTO+" = '"+photoPath+"'"+
//                " WHERE " +DatabaseConstanta.ID +
//                "= "+id;
//
//        System.out.println(querry_update);
//        db.execSQL(querry_update);

        //cara 2 ... content value
        ContentValues content = new ContentValues();
        content.put(DatabaseConstanta.NIM,nim);
        content.put(DatabaseConstanta.NAMA_LENGKAP,nama);
        content.put(DatabaseConstanta.TANGGAL_LAHIR,tanggal_lahir);
        content.put(DatabaseConstanta.ALAMAT,alamat);
        content.put(DatabaseConstanta.JURUSAN,jurusan);
        content.put(DatabaseConstanta.GENDER,gender);
        content.put(DatabaseConstanta.PATH_PHOTO,photoPath);
        db.update(DatabaseConstanta.TABEL_BIODATA,content,DatabaseConstanta.ID+"="+Integer.toString(id),null);
        finish();
    }

    void ambilFotoProfil(){
        ImagePicker.create((Activity)context)
                .single()
                .folderMode(true)
                .limit(1)
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(ImagePicker.shouldHandle(requestCode,resultCode,data)){
            Image photo = ImagePicker.getFirstImageOrNull(data);
            if(photo!=null){
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(photo.getPath(),options);

                //set image
                Glide.with(context)
                        .load(bitmap)
                        .into(inputFotoMhs);

                //yg ingin di simpan itu path nya :

                PHOTO_PATH = photo.getPath();

            }
        }
    }
}
