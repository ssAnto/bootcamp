package com.example.example.menues;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.example.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import mumayank.com.airlocationlibrary.AirLocation;

public class GoogleMapsAPIActivity extends FragmentActivity implements OnMapReadyCallback {

    private Context context=this;
    private TextView latitude,longitude,altitude;
    private Button btnGetCurrentLocation;
    private AirLocation airLocation;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps_api);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //init
        latitude=findViewById(R.id.latitude);
        longitude=findViewById(R.id.longitude);
        altitude=findViewById(R.id.altitude);
        btnGetCurrentLocation=findViewById(R.id.btnMyCurrentLocation);

        btnGetCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableLocationRequest();
                getMyCurrentLocation();
                LatLng position = new LatLng(Double.parseDouble(latitude.getText().toString()),Double.parseDouble(longitude.getText().toString()));
                mMap.addMarker(new MarkerOptions().position(position).title("Posisi Saya Saat ini"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
            }
        });
    }


    private void getMyCurrentLocation() {
        airLocation=new AirLocation((AirLocationActivity) context,
                true,
                true,
                new AirLocation.Callbacks() {
                    @Override
                    public void onSuccess(Location location) {
                        String latitude_value = String.valueOf(location.getLatitude());
                        String longitude_value = String.valueOf(location.getLongitude());
                        String altitude_value = String.valueOf(location.getAltitude());

                        latitude.setText(latitude_value);
                        longitude.setText(longitude_value);
                        altitude.setText(altitude_value);
                    }

                    @Override
                    public void onFailed(AirLocation.LocationFailedEnum locationFailedEnum) {
                        Toast.makeText(context, "Gagal Mengambil Lokasi", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        airLocation.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        airLocation.onActivityResult(requestCode,resultCode,data);
    }

    private boolean checkIsGPSEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            return  true;
        }else{
            return  false;
        }
    }

    private void enableLocationRequest() {
        if (!checkIsGPSEnabled()) {
            showLocationSetting();
        }
    }
    private  void showLocationSetting(){
        AlertDialog.Builder info = new AlertDialog.Builder(context);

        info.setMessage("Anda Belum Mengaktifkan GPS Aktifkan Sekarang !!!")
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //untuk panggil menu setting
                        Intent setting = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(setting);
                    }
                })
                .setNegativeButton("TDK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setCancelable(false);
        info.create().show();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
