package com.example.example.menues.stack;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.example.R;

public class StackOneActivity extends AppCompatActivity {

    private Context context=this;
    private Button aaa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stack_one);

        aaa=findViewById(R.id.next);
        aaa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context,StackTwoActivity.class));
            }
        });
    }
}

