package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.example.R;
import com.example.example.models.PhotoModel;
import com.example.example.utilities.TemporaryData;

public class DetailPhotoActivity extends AppCompatActivity {
    private Context context = this;
    private ImageView imageView ;
    private TextView albumId,photoId,title;
    private PhotoModel tmp = TemporaryData.getPhotoModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_photo);

        //init
        imageView = findViewById(R.id.showImage);
        albumId = findViewById(R.id.albumId);
        photoId = findViewById(R.id.photoID);
        title = findViewById(R.id.title);

        albumId.setText(Integer.toString(tmp.getAlbungId()));
        photoId.setText(Integer.toString(tmp.getId()));
        title.setText(tmp.getTitle());
        Glide.with(context)
                .load(tmp.getUrl())
                .into(imageView);
    }
}
