package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.example.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MultiLevelParsingActivity extends AppCompatActivity {
    private Context context = this;
    private TextView totalNews,snippedNews, totalMedia, cropName;
    private ImageView legacyXlarge;
    private ProgressBar loading;
    private LinearLayout contentNews;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_level_parsing);

        //init
        totalNews = findViewById(R.id.total_news);
        totalMedia = findViewById(R.id.total_media);
        snippedNews = findViewById(R.id.snippedNews);
        cropName = findViewById(R.id.cropName);
        legacyXlarge = findViewById(R.id.legacyXLarge);
        loading= findViewById(R.id.loadingBar);
        contentNews = findViewById(R.id.contentNews);
        loadNYTimeAPI();
    }

    void loadNYTimeAPI(){
        contentNews.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        String url = "https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=9b693ffaa5fe451090146e5c90fbed78&q=indonesia&page=0";

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        contentNews.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);

                        //parsing
                        //level 1 = respon;
                        try {
                            //lv 1
                            JSONObject responObjek = response.getJSONObject("response");

                            //lv 2
                            JSONArray docArray = responObjek.getJSONArray("docs");

                            //set total news
                            int count_news = docArray.length();
                            totalNews.setText(Integer.toString(count_news));

                            //lv3 = snipped
                            JSONObject lv3 = docArray.getJSONObject(0);
                            String snippedValue = lv3.getString("snippet");
                            snippedNews.setText(snippedValue);

                            //lv3 = multimedia
                            int count_multimedia = lv3.getJSONArray("multimedia").length();
                            totalMedia.setText(Integer.toString(count_multimedia));

                            //lv4 = crop name
                            JSONArray lv4 = lv3.getJSONArray("multimedia");
                            JSONObject Lv4_objek = lv4.getJSONObject(2);
                            String Crop_value = Lv4_objek.getString("crop_name");
                            cropName.setText(Crop_value);

                            //lv5 = xLarge
                            Lv4_objek = lv4.getJSONObject(0);
                            JSONObject LV5_Legacy =Lv4_objek.getJSONObject("legacy");
                            String xLarge_value = "https://www.nytimes.com/"+LV5_Legacy.getString("xlarge");
                            Glide.with(context)
                                    .load(xLarge_value)
                                    .into(legacyXlarge);
                        }catch (JSONException e){
                            System.out.println("Error : "+e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.setVisibility(View.GONE);
                        System.out.println("Error : "+error.getMessage());
                    }
                }
        );
        Volley.newRequestQueue(context).add(request);
    }
}
