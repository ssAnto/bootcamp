package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;

import com.example.example.R;
import com.example.example.adapters.TabLayoutAdapter;
import com.google.android.material.tabs.TabLayout;

public class TabbedMenuActivity extends AppCompatActivity {
    private Context context=this;
    private TabLayout slidingTabs;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed_menu);

        //init
        slidingTabs = findViewById(R.id.slidingTab);
        viewPager = findViewById(R.id.viewPager);

        TabLayoutAdapter adapter = new TabLayoutAdapter(context,getSupportFragmentManager(),slidingTabs);
        viewPager.setAdapter(adapter);
        slidingTabs.setupWithViewPager(viewPager);
    }
}
