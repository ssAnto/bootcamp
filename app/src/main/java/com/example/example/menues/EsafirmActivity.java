package com.example.example.menues;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.imageloader.ImageLoader;
import com.esafirm.imagepicker.features.imageloader.ImageType;
import com.esafirm.imagepicker.model.Image;
import com.example.example.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class EsafirmActivity extends AppCompatActivity {
    private Context context = this;
    private Button buttonCameraEsafirm,buttonGaleryEsafirm;
    private ImageView reviewPhotofromCameraEsafirm;
    private LinearLayout sliderImageEsafirm;

    private int ID_REQUEST_CAMERA = 3;
    private int ID_REQUEST_GALERY=4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esafirm);

        //init
        buttonCameraEsafirm=findViewById(R.id.buttonCameraEsafirm);
        buttonGaleryEsafirm=findViewById(R.id.buttonGaleryEsafirm);
        reviewPhotofromCameraEsafirm = findViewById(R.id.reviewPhotofromCameraEsafirm);
        sliderImageEsafirm = findViewById(R.id.sliderImageEsafirm);

        buttonGaleryEsafirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFromGalery();
            }
        });

        buttonCameraEsafirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFromCamera();
            }
        });
    }

    void getImageFromGalery(){
        ImagePicker.create((Activity) context)
            .multi()
            .limit(20)
            .folderMode(true)
            .start(ID_REQUEST_GALERY);
    }

    void  getImageFromCamera(){
        //permision telah di handle oleh esafirmnya ...
        //ni harus pake gldie versi 4.5.0 ... kalo pake versi glide 4.10.0 error dia ..
        
        ImagePicker.cameraOnly().start((Activity) context,ID_REQUEST_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("Datanya adalah "+data);
        if(requestCode==ID_REQUEST_CAMERA){
            Image image = ImagePicker.getFirstImageOrNull(data);
            if(image != null){
                //lakukan conversinya ...
//                BitmapFactory.Options options = new BitmapFactory.Options();
//                Bitmap bitmap = BitmapFactory.decodeFile(image.getPath(),options);

                Glide.with(context).load(image.getPath()).into(reviewPhotofromCameraEsafirm);
            }
        }else if(requestCode==ID_REQUEST_GALERY && data!=null){
            //untuk galery
            List<Image> images = ImagePicker.getImages(data);
            for (int i = 0; i < images.size(); i++) {
                if(images.get(i) != null ){

                    //tambahkan image view di dalam layout yg tadinya gak ada isi
                    ImageView imageView = new ImageView(context);
                    Glide.with(context)
                            .load(images.get(i).getPath())
                            //.apply(new RequestOptions().override(600,600))
                            .into(imageView);
//

                    sliderImageEsafirm.addView(imageView);
                }
            }

        }
    }
//    class PicassoImageLoader implements ImageLoader{
//        private int MAX_WIDTH=600;
//        private int MAX_HEIGHT=600;
//
//        @Override
//        public void loadImage(String path, ImageView imageView, ImageType imageType) {
//            int placeholderResId;
//
//            if(imageType == ImageType.FOLDER){
//                placeholderResId=R.drawable.ef_folder_placeholder;
//            }else{
//                placeholderResId=R.drawable.ef_image_placeholder;
//            }
//
//            Picasso.get()
//                    .load(new File(path))
//                    .into();
//        }
//    }
}
