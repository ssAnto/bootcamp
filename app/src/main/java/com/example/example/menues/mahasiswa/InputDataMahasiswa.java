package com.example.example.menues.mahasiswa;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.VoiceInteractor;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.example.example.R;
import com.example.example.database_utilities.DatabaseConstanta;
import com.example.example.database_utilities.DatabaseMahasiswaHelper;
import com.example.example.utilities.Konstanta;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class InputDataMahasiswa extends AppCompatActivity {
    private Context context= this;

    private EditText inputNama,inputNomorInduk,inputAlamatMahasiswa;
    private RadioGroup genderMahasiswa;
    private ImageView inputFotoMhs;
    private FloatingActionButton buttonSimpanDataMhs;
    private Spinner spinnerJurusanMHS;
    private TextView tanggalLahir;
    private static String PHOTO_PATH ="";

    private DatabaseMahasiswaHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_data_mahasiswa);

        //init

        inputNama = findViewById(R.id.inputNama);
        inputNomorInduk = findViewById(R.id.inputNomorInduk);
        inputAlamatMahasiswa = findViewById(R.id.inputAlamatMahasiswa);
        genderMahasiswa = findViewById(R.id.genderMahasiswa);
        inputFotoMhs = findViewById(R.id.inputFotoMhs);
        buttonSimpanDataMhs = findViewById(R.id.buttonSimpanDataMhs);
        tanggalLahir=findViewById(R.id.tanggalLahir);
        spinnerJurusanMHS = findViewById( R.id.spinnerJurusanMHS);

        buttonSimpanDataMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validasiInputDataMahasiswa();
            }
        });

        inputFotoMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ambilFotoProfil();
            }
        });

        isiSpinerJurusan();
        setTanggalLahir();
    }

    void  setTanggalLahir(){
        Calendar today = Calendar.getInstance();
        final int yearnow = today.get(Calendar.YEAR);
        final int monthnow = today.get(Calendar.MONTH);
        final int daynow = today.get(Calendar.DATE);

        tanggalLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog tanggal_lahir = new DatePickerDialog(
                        context,
                        R.style.CustomDatePicker,
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth){
                                Calendar selectedDate = Calendar.getInstance();
                                selectedDate.set(year,month,dayOfMonth);

                                //konversi ke string
                                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                String tanggal = formatter.format(selectedDate.getTime());

                                tanggalLahir.setText(tanggal);
                            }
                        },
                        yearnow,
                        monthnow,
                        daynow
                );
                tanggal_lahir.show();
            }
        });
    }

    void validasiInputDataMahasiswa(){
        String nim = inputNomorInduk.getText().toString().trim();
        String nama=inputNama.getText().toString().trim();
        String tanggal_lahir = tanggalLahir.getText().toString().trim();
        String alamat = inputAlamatMahasiswa.getText().toString().trim();

        int selectedGender = genderMahasiswa.getCheckedRadioButtonId();;
        int jurusan = spinnerJurusanMHS.getSelectedItemPosition();

        String jurusanSelected = Konstanta.ARRAY_JURUSAN[jurusan];


        String tmp = "Belum Di Isi !!!!!!!";
        if(nim.length() == 0){
            Toast.makeText(context, "NIM "+tmp, Toast.LENGTH_SHORT).show();
        }else if(nama.length() == 0){
            Toast.makeText(context, "NAMA "+tmp, Toast.LENGTH_SHORT).show();
        }else if(tanggal_lahir.length()==0){
            Toast.makeText(context, "Tanggal Lahir "+tmp, Toast.LENGTH_SHORT).show();
        }else if(alamat.length()==0){
            Toast.makeText(context, "Alamat "+tmp, Toast.LENGTH_SHORT).show();
        }else if(jurusan==0){
            Toast.makeText(context, "Jurusan  "+tmp, Toast.LENGTH_SHORT).show();
        }else if(selectedGender==-1){
            Toast.makeText(context, "Gender "+tmp, Toast.LENGTH_SHORT).show();
        }else if(PHOTO_PATH.length()==0){
            Toast.makeText(context, "Foto Profil "+tmp, Toast.LENGTH_SHORT).show();
        }else{
            //valit ...
            String gender = "Perempuan";
            if( selectedGender == R.id.gender_l){
                gender="Laki-Laki";
            }

            System.out.println(selectedGender);
            System.out.println(gender);

            insertDatabaseMahasiswa(
                    nim,
                    nama,
                    tanggal_lahir,
                    alamat,
                    jurusanSelected,
                    gender,
                    PHOTO_PATH
            );

        }
    }

    void insertDatabaseMahasiswa(
            String nim,
            String nama,
            String tanggal_lahir,
            String alamat,
            String jurusan,
            String gender,
            String photoPath) {

        databaseHelper = new DatabaseMahasiswaHelper(context);
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        String querry_insert = "INSERT INTO '"+ DatabaseConstanta.TABEL_BIODATA+"' " +
                "('" +DatabaseConstanta.NIM +
                "','" +DatabaseConstanta.NAMA_LENGKAP +
                "','" +DatabaseConstanta.TANGGAL_LAHIR+
                "','" +DatabaseConstanta.ALAMAT+
                "','" +DatabaseConstanta.JURUSAN+
                "','" +DatabaseConstanta.GENDER+
                "','" +DatabaseConstanta.PATH_PHOTO+
                "') VALUES ('" +nim+
                "','" +nama+
                "','" +tanggal_lahir+
                "','" +alamat+
                "','" +jurusan+
                "','" +gender+
                "','" +photoPath+
                "')";

        db.execSQL(querry_insert);
        finish();
    }

    void isiSpinerJurusan(){
        ArrayAdapter<String> adapterJurusan = new ArrayAdapter<>(
                context,
                android.R.layout.simple_spinner_item,
                Konstanta.ARRAY_JURUSAN
        );
        adapterJurusan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerJurusanMHS.setAdapter(adapterJurusan);
    }


    void ambilFotoProfil(){
        ImagePicker.create((Activity)context)
                .single()
                .folderMode(true)
                .limit(1)
                .start();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(ImagePicker.shouldHandle(requestCode,resultCode,data)){
            Image photo = ImagePicker.getFirstImageOrNull(data);
            if(photo!=null){
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(photo.getPath(),options);

                //set image
                Glide.with(context)
                        .load(bitmap)
                        .into(inputFotoMhs);

                //yg ingin di simpan itu path nya :

                PHOTO_PATH = photo.getPath();

            }
        }
    }
}


