package com.example.example.menues.mahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.example.R;
import com.example.example.database_utilities.DatabaseConstanta;
import com.example.example.database_utilities.DatabaseMahasiswaHelper;
import com.example.example.utilities.Konstanta;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;
import java.util.List;

public class DetailMahasiswaActivity extends AppCompatActivity {
    private Context context = this;
    private TextView labelNamaMHS,labelNIMMHS,labelTGLLahirMHS,labelGenderMHS,labelJurusanMHS,labelAlamatMHS;
    private ImageView fotoProfilMhs;
    private int ID_MHS;
    private  String PATH="file://";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_mahasiswa);

        //init

        labelAlamatMHS = findViewById(R.id.labelAlamatMHS);
        labelNIMMHS = findViewById(R.id.labelNIMMHS);
        labelTGLLahirMHS = findViewById(R.id.labelTGLLahirMHS);
        labelGenderMHS = findViewById(R.id.labelGenderMHS);
        labelJurusanMHS = findViewById(R.id.labelJurusanMHS);
        labelNamaMHS = findViewById(R.id.labelNamaMHS);

        fotoProfilMhs=findViewById(R.id.fotoProfilMhs);

        //ambil intent extra
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            ID_MHS=bundle.getInt(DatabaseConstanta.ID);
            System.out.println(ID_MHS);
        }
        readDataDetailMahasiswa();

        fotoProfilMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZoomGambar();
            }
        });
    }

    void ZoomGambar(){
        Fresco.initialize(context);
        List<String> uri = new ArrayList<>();
        uri.add(PATH);
        new ImageViewer.Builder(context,uri)
                .setStartPosition(0)
                .show();
    }

    void readDataDetailMahasiswa(){
        DatabaseMahasiswaHelper databaseMahasiswaHelper = new DatabaseMahasiswaHelper(context);
        SQLiteDatabase db = databaseMahasiswaHelper.getReadableDatabase();

        //querry
        String query="SELECT * FROM " +DatabaseConstanta.TABEL_BIODATA+
                " WHERE " +DatabaseConstanta.ID+
                " = "+ID_MHS;

        Cursor cursor = db.rawQuery(query,null);
        if(cursor.getCount() == 1){
            cursor.moveToFirst();
            String nim = cursor.getString(1);
            String nama = cursor.getString(2);
            String gender = cursor.getString(3);
            String tanggal = cursor.getString(4);
            String alamat =cursor.getString(5);
            String jurusan = cursor.getString(6);
            String path = cursor.getString(7);

            labelNamaMHS.setText(nama);
            labelJurusanMHS.setText(jurusan);
            labelNIMMHS.setText(nim);
            labelGenderMHS.setText(gender);
            labelTGLLahirMHS.setText(tanggal);
            labelAlamatMHS.setText(alamat);
            PATH+=path;
            Glide.with(context)
                    .load(path)
                    .into(fotoProfilMhs);
        }
    }
}
