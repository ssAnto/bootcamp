package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.example.R;
import com.example.example.utilities.Loading;

public class VolleyActivity extends AppCompatActivity {
    private Context context = this;
    private Button buttonLoadString;
    private TextView result;
    private ImageView imgFromWeb;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volley);

        //init
        buttonLoadString=findViewById(R.id.buttonLoadString);
        result=findViewById(R.id.textResult);
        imgFromWeb=findViewById(R.id.imgFromWeb);
        progressBar=findViewById(R.id.progress_bar);
        buttonLoadString.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ambilString();
                ambilImage();
            }
        });
    }

    void ambilImage(){
        progressBar.setVisibility(View.VISIBLE);
        imgFromWeb.setVisibility(View.GONE);
        final String imageUrl="https://pics.me.me/gambar-gambar-legend-sewaktu-cue-masih-sd-dullum-matahari-burung-4074479.png";
       ImageRequest request = new ImageRequest(
               imageUrl,
               new Response.Listener<Bitmap>() {
                   @Override
                   public void onResponse(Bitmap response) {
                       imgFromWeb.setImageBitmap(response);
                       progressBar.setVisibility(View.GONE);
                       imgFromWeb.setVisibility(View.VISIBLE);
                   }
               },
               0,
               0,
               ImageView.ScaleType.FIT_CENTER,
               Bitmap.Config.ARGB_8888,
               new Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error) {
                       System.out.println("Error loading Image = "+error.getMessage());
                       progressBar.setVisibility(View.GONE);
                       imgFromWeb.setVisibility(View.VISIBLE);
                   }
               }

       );
       Volley.newRequestQueue(context).add(request);
    }

    void ambilString(){
        final ProgressDialog loading = Loading.loadingAnimationdanText(context,
                "Tunggu Oi !!!!!");
        loading.show();
        //ambil pake volley
        String url = "https://jsonplaceholder.typicode.com/posts";
        final StringRequest requestString = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        result.setText(response);
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Error "+error.getMessage(), Toast.LENGTH_SHORT).show();
                        //tampilkan di terminal
                        System.out.println("Error "+error.getMessage());
                        //buat log
                        Log.e("error","Error "+error.getMessage());
                        loading.dismiss();
                    }
                }
        );

        Volley.newRequestQueue(context).add(requestString);
    }
}
