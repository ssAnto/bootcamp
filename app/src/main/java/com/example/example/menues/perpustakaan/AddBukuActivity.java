package com.example.example.menues.perpustakaan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.example.R;
import com.example.example.database_utilities.DatabasePerpustakaanHelper;
import com.example.example.database_utilities.DatabasePerpustakaanQuerryHelper;
import com.example.example.models.BukuModel;
import com.example.example.utilities.Tools;

public class AddBukuActivity extends AppCompatActivity {

    private Context context = this;
    private EditText inputJudulBuku, inputKategoriBuku,inputPengarangBuku,inputPenerbitBuku,inputHargaBuku,inputStockBuku;
    private Button btnAddBuku;

    private DatabasePerpustakaanHelper databasePerpustakaanHelper = new DatabasePerpustakaanHelper(context);
    private DatabasePerpustakaanQuerryHelper querryHelper = new DatabasePerpustakaanQuerryHelper(databasePerpustakaanHelper);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_buku);

        //init
        inputJudulBuku = findViewById(R.id.inputJudulBuku);
        inputPengarangBuku = findViewById(R.id.inputPengarangBuku);
        inputPenerbitBuku = findViewById(R.id.inputPenerbitBuku);
        inputKategoriBuku = findViewById(R.id.inputKategoriBuku);
        inputHargaBuku = findViewById(R.id.inputHargaBuku);
        inputStockBuku = findViewById(R.id.inputStockBuku);

        btnAddBuku = findViewById(R.id.btnAddBuku);

        btnAddBuku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.hideKeyboard(context,getCurrentFocus().getWindowToken());

                validasiInput();
                finish();
            }
        });

    }

    private void validasiInput() {

        String judul = inputJudulBuku.getText().toString().trim();
        String kategori = inputKategoriBuku.getText().toString().trim();
        String pengarang = inputPengarangBuku.getText().toString().trim();
        String penerbit = inputPenerbitBuku.getText().toString().trim();
        String  harga = inputHargaBuku.getText().toString().trim();
        String  stock = inputStockBuku.getText().toString().trim();
        String w="Belum Di Isi Oi !!!!!";
        if(judul.length() == 0){
            Toast.makeText(context, "Judul "+w, Toast.LENGTH_SHORT).show();
        }else if(kategori.length() == 0){
            Toast.makeText(context, "Kategori "+w, Toast.LENGTH_SHORT).show();
        }else if(pengarang.length() == 0){
            Toast.makeText(context, "Pengarang "+w, Toast.LENGTH_SHORT).show();
        }else if(penerbit.length() == 0){
            Toast.makeText(context, "Penerbit "+w, Toast.LENGTH_SHORT).show();
        }else if(harga.isEmpty()){
            Toast.makeText(context, "Harga "+w, Toast.LENGTH_SHORT).show();
        }else if(stock.isEmpty()){
            Toast.makeText(context, "Stock  "+w, Toast.LENGTH_SHORT).show();
        }else{
            BukuModel bukuModel =  new BukuModel();
            bukuModel.setJudul_buku(judul);
            bukuModel.setPenerbit_buku(penerbit);
            bukuModel.setStok(Integer.parseInt(stock));
            bukuModel.setHarga(Integer.parseInt(harga));
            bukuModel.setKategori_buku(kategori);
            bukuModel.setPengarang_buku(pengarang);

            querryHelper.addBuku(bukuModel);
        }
    }
}
