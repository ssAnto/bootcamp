package com.example.example.menues;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.example.R;

import mumayank.com.airlocationlibrary.AirLocation;

public class AirLocationActivity extends AppCompatActivity {
    private Context context = this;
    private TextView latitude,longitude;
    private Button btnGetAirLocation;
    private AirLocation airLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_air_location);

        //init
        latitude=findViewById(R.id.latitude);
        longitude=findViewById(R.id.longitude);
        btnGetAirLocation=findViewById(R.id.btnGetAirLocation);

        btnGetAirLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableLocationRequest();
                getMyCurrentLocation();
            }
        });
    }

    private void getMyCurrentLocation() {
        airLocation=new AirLocation((AirLocationActivity) context,
                true,
                true,
                new AirLocation.Callbacks() {
                    @Override
                    public void onSuccess(Location location) {
                        String latitude_value = String.valueOf(location.getLatitude());
                        String longitude_value = String.valueOf(location.getLongitude());

                        latitude.setText(latitude_value);
                        longitude.setText(longitude_value);
                    }

                    @Override
                    public void onFailed(AirLocation.LocationFailedEnum locationFailedEnum) {
                        Toast.makeText(context, "Gagal Mengambil Lokasi", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        airLocation.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        airLocation.onActivityResult(requestCode,resultCode,data);
    }

    private boolean checkIsGPSEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            return  true;
        }else{
            return  false;
        }
    }

    private void enableLocationRequest() {
        if (!checkIsGPSEnabled()) {
            showLocationSetting();
        }
    }
    private  void showLocationSetting(){
            AlertDialog.Builder info = new AlertDialog.Builder(context);

            info.setMessage("Anda Belum Mengaktifkan GPS Aktifkan Sekarang !!!")
                    .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //untuk panggil menu setting
                            Intent setting = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(setting);
                        }
                    })
                    .setNegativeButton("TDK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setCancelable(false);
            info.create().show();
        }
}
