package com.example.example.menues;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.example.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class LocationTrackingActivity extends AppCompatActivity {
    private Context context = this;
    private TextView latitude,longitude;
    private Button start,stop;
    private int ID_RQ_LOCATION = 1234;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_tracking);

        //init

        latitude = findViewById( R.id.latitude);
        longitude = findViewById(R.id.longitude);

        start = findViewById(R.id.btnStartLocation);
        stop = findViewById(R.id.btnStopLocation);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkPermissionLocation()){
                    start.setEnabled(!start.isEnabled());
                    stop.setEnabled(!stop.isEnabled());
                    initLocationProvider();


                }
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeLocationProvider();
                start.setEnabled(!start.isEnabled());
                stop.setEnabled(!stop.isEnabled());

            }
        });
    }

    private void removeLocationProvider() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    private void initLocationProvider() {
        if(fusedLocationProviderClient == null){
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
            fusedLocationProviderClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.myLooper()
            );
        }
    }

    private boolean checkPermissionLocation() {
        boolean hasil = true;
        int currentAPIversion = Build.VERSION.SDK_INT;
        if(currentAPIversion > Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) !=  PackageManager.PERMISSION_GRANTED
            || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                //request permisiio
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION},ID_RQ_LOCATION);
                hasil=false;
            }
        }

        return hasil;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == ID_RQ_LOCATION){
            if(grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                enableLocationRequest();
            }
        }
    }

    private void enableLocationRequest() {
        if(checkPermissionLocation()){
            buildLocationRequest();
            buildLocationCallback();

            if(!checkIsGPSEnabled()){
                showLocationSetting();
            }
        }
    }

    private boolean checkIsGPSEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            return  true;
        }else{
            return  false;
        }
    }

    private void buildLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(locationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(2000);
        locationRequest.setSmallestDisplacement(10f);
    }

    private  void buildLocationCallback(){
        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
//                super.onLocationResult(locationResult);

                for(Location location : locationResult.getLocations()){
                    String latitude_value = Double.toString(location.getLatitude());
                    String longitude_value = Double.toString(location.getLongitude());

                    latitude.setText(latitude_value);
                    longitude.setText(longitude_value);
                }

            }
        };
    }

    private  void showLocationSetting(){
        AlertDialog.Builder info = new AlertDialog.Builder(context);

        info.setMessage("Anda Belum Mengaktifkan GPS Aktifkan Sekarang !!!")
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //untuk panggil menu setting
                        Intent setting = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(setting);
                    }
                })
                .setNegativeButton("TDK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setCancelable(false);
        info.create().show();
    }
}
