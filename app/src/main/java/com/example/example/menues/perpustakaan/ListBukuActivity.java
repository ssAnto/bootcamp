package com.example.example.menues.perpustakaan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.example.R;
import com.example.example.adapters.ListBukuAdapter;
import com.example.example.adapters.ListMhsAdapter;
import com.example.example.database_utilities.DatabasePerpustakaanHelper;
import com.example.example.database_utilities.DatabasePerpustakaanQuerryHelper;
import com.example.example.models.BukuModel;
import com.example.example.utilities.Tools;

import java.util.ArrayList;
import java.util.List;

public class ListBukuActivity extends AppCompatActivity {
    private Context context = this;
    private TextView totalBuku,kosong;
    private EditText inputKeyword;
    private Button btnCariBuku;
    private RecyclerView listBukuRecicler;

    private DatabasePerpustakaanHelper dbHelper = new DatabasePerpustakaanHelper(context);
    private DatabasePerpustakaanQuerryHelper querryHelper = new DatabasePerpustakaanQuerryHelper(dbHelper);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_buku);

        //init
        totalBuku = findViewById(R.id.totalBuku);
        inputKeyword = findViewById(R.id.inputKeyword);
        btnCariBuku = findViewById(R.id.btnCariBuku);
        listBukuRecicler = findViewById(R.id.listBukuRecicler);
        kosong=findViewById(R.id.kosong);

        btnCariBuku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.hideKeyboard(context, getCurrentFocus().getWindowToken());
                validasiKeyword();
            }
        });

        listBukuRecicler.setLayoutManager(new LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL,false));


        //bagusnya dipakai saat datanya ada di local storedge
        inputKeyword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count!=0) {
                    validasiKeyword();
                }else{
                    tampilkanSemuaBuku();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tampilkanSemuaBuku();

    }

    public void tampilkanSemuaBuku(){

        ArrayList<BukuModel> bukuModels = querryHelper.getAllListBuku();
        if(bukuModels.size() > 0 && bukuModels !=null) {
            ListBukuAdapter adapter = new ListBukuAdapter(context, bukuModels);
            listBukuRecicler.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            totalBuku.setText(Integer.toString(bukuModels.size()));
            kosong.setVisibility(View.GONE);
            listBukuRecicler.setVisibility(View.VISIBLE);
        }else {
            kosong.setVisibility(View.VISIBLE);
            kosong.setText("List Buku Perpustakaan Kosong !!!!");
            listBukuRecicler.setVisibility(View.GONE);
            totalBuku.setText("0");
        }

    }

    private void validasiKeyword() {
        String keyword = inputKeyword.getText().toString();

        if(keyword.isEmpty()){
            //Toast.makeText(context, "KeyWord tidak boleh kosong !!! ", Toast.LENGTH_SHORT).show();
            tampilkanSemuaBuku();
        }else{
            //search
            lakukanSearching();
        }
    }

    private void lakukanSearching() {
        String keyword=inputKeyword.getText().toString();
        ArrayList<BukuModel> bukuModels = querryHelper.searchListBuku(keyword);
        if(bukuModels.size() > 0 && bukuModels !=null) {
            ListBukuAdapter adapter = new ListBukuAdapter(context, bukuModels);
            listBukuRecicler.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            totalBuku.setText(Integer.toString(bukuModels.size()));
            kosong.setVisibility(View.GONE);
            listBukuRecicler.setVisibility(View.VISIBLE);
        }else {
            kosong.setVisibility(View.VISIBLE);
            kosong.setText(keyword+" Tidak Ditemukan !!!!");
            totalBuku.setText("0");
            listBukuRecicler.setVisibility(View.GONE);
        }

    }
}
