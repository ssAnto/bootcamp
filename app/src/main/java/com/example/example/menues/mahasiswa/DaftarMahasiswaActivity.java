package com.example.example.menues.mahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.example.example.R;
import com.example.example.adapters.ListMhsAdapter;
import com.example.example.database_utilities.DatabaseConstanta;
import com.example.example.database_utilities.DatabaseMahasiswaHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class DaftarMahasiswaActivity extends AppCompatActivity {
    private Context context = this;
    private RecyclerView listMahasiswa;
    private FloatingActionButton buttonInputDataMahasiswa;

    private DatabaseMahasiswaHelper databaseHelper;
    private List<String > namaMhs = new ArrayList<>();
    private List<Integer> idMhs = new ArrayList<>();
    private List<String> PATH = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_mahasiswa);

        //init
        listMahasiswa = findViewById(R.id.listMahasiswa);
        listMahasiswa.setLayoutManager(new LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL,false));

        buttonInputDataMahasiswa = findViewById(R.id.buttonInputDataMahasiswa);
        buttonInputDataMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pindahKeActivityInputDataMahasiswa();
            }
        });

        readDataMahasiswa();
    }
    void readDataMahasiswa(){
        databaseHelper=new DatabaseMahasiswaHelper(context);
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        String queryRead = "SELECT * FROM "+ DatabaseConstanta.TABEL_BIODATA;
        Cursor cursor = db.rawQuery(queryRead, null);

        //ambil data dari SQLite yg berupa cursor dan tampung dalam list namaMhs dan list idMhs
        if(cursor.getCount()==0){
            Toast.makeText(context,"Data tidak Ditemukan",Toast.LENGTH_LONG).show();
        }else{
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                String nama = cursor.getString(2);
                int id = cursor.getInt(0);
                String path = cursor.getString(7);
                namaMhs.add(nama);
                idMhs.add(id);
                PATH.add(path);
            }
        }
        //tampilkan data
        showDataMahasiswa();
    }

    void showDataMahasiswa(){
        ListMhsAdapter adapter = new ListMhsAdapter(context,namaMhs,idMhs,PATH);
        listMahasiswa.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }

    void pindahKeActivityInputDataMahasiswa(){
        Intent intent = new Intent(context, InputDataMahasiswa.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshTampilan();
    }

   public void refreshTampilan(){
        //kosongkan list nya dulu ..
        namaMhs = new ArrayList<>();
        idMhs = new ArrayList<>();
        PATH = new ArrayList<>();
        readDataMahasiswa();
    }
}
