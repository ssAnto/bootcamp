package com.example.example.menues;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.example.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class CameraActivity extends AppCompatActivity {
    private Context context = this;
    private Button take_picture;
    private ImageView previewPhoto;

    //bebas .. ini untuk mengetahui siapa yg panggil ..
    private int ID_PEMANGGIL = 1;
    //untuk id request permission camera
    private  int PERMISSION_REQUEST_FOR_CAMERA = 11;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        //init
        take_picture = findViewById(R.id.buttonCamera);
        previewPhoto = findViewById(R.id.reviewPhotofromCamera);

        take_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ambil image dari kamera
                if(checkPerissionCamera())
                {
                    ambilImagedariCamera();
                }
            }
        });
    }

    private boolean checkPerissionCamera(){
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion >= Build.VERSION_CODES.M){
            //android >=6
            if(checkSelfPermission(Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
                // tampilkan dialog request permision
                requestPermissions(new String[] {Manifest.permission.CAMERA},
                        PERMISSION_REQUEST_FOR_CAMERA);
                return false;
            }else{
                return true;
            }
        }else{
            //android jadul android <6
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode==PERMISSION_REQUEST_FOR_CAMERA){
            if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                ambilImagedariCamera();
            }else{
                //tidak di ijinkan ....
                Toast.makeText(context, "Anda Harus Memberikan ijin Akses Kamera untuk menggunakan feature ini",Toast.LENGTH_LONG).show();
            }
        }
    }

    void ambilImagedariCamera(){
        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intentCamera,ID_PEMANGGIL);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //cek apakah request code ini cocok degn id pemanggil ..
        if(requestCode == ID_PEMANGGIL && resultCode == Activity.RESULT_OK){
            //ambil imagenya ...
            showImageFromCamera(data);
        }else{
            //gagal ...
            Toast.makeText(context,"Gagal Mengambil image dari camera", Toast.LENGTH_LONG).show();
        }
    }

    void  showImageFromCamera(Intent data){
        //proses conversi dari data intent menjadi bitmap
        if(data!=null){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            final List<Bitmap> bitmaps = new ArrayList<>();


            //konversi ke gambar cara manual ...
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,outputStream);

            previewPhoto.setImageBitmap(bitmap);
            bitmaps.add(bitmap);

            //kalo di klik tampilkan besarnya ..
            previewPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fresco.initialize(context);
                    new ImageViewer.Builder(context,bitmaps)
                            .setStartPosition(0)
                            .show();
                }
            });

            //ganti action toolbar
            bypassActionToolbar();
        }
    }

    void bypassActionToolbar(){
        //panggil toolbar dulu
        ActionBar toolbar = getSupportActionBar();
        toolbar.setTitle("Badu Di gambar ini !!!!");


        //ni untuk munculkan tombol backnya ...
        toolbar.setDisplayHomeAsUpEnabled(true);
        toolbar.setDisplayShowHomeEnabled(true);
    }

    //listener tombol back

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
