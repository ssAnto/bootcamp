package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.example.R;
import com.example.example.utilities.mvchart.DayAxisValueFormatter;
import com.example.example.utilities.mvchart.MyValueFormatter;
import com.example.example.utilities.mvchart.XYMarkerView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.model.GradientColor;

import java.util.ArrayList;
import java.util.List;

public class ChartActivity extends AppCompatActivity implements OnChartValueSelectedListener,
        SeekBar.OnSeekBarChangeListener{

    private Context context = this;

    private BarChart chart1;
    private SeekBar seekBarX, seekBarY;
    private TextView tvXMax, tvYMax;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        //init
        chart1 = findViewById(R.id.chart1);
        seekBarX = findViewById(R.id.seekbar1);
        seekBarY = findViewById(R.id.seekbar2);
        tvXMax = findViewById(R.id.tvXMax);
        tvYMax = findViewById(R.id.tvYMax);

        //set listener
        chart1.setOnChartValueSelectedListener(this);
        seekBarX.setOnSeekBarChangeListener(this);
        seekBarY.setOnSeekBarChangeListener(this);

        configChart();
    }

    private void configChart() {
        //description
        chart1.getDescription().setEnabled(false);

        //max entries
        chart1.setMaxVisibleValueCount(60);

        //scalling chart false, enable by seekbar
        chart1.setPinchZoom(false);

        //grid false
        chart1.setDrawGridBackground(false);

        //sumbu x & y
        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(chart1);

        XAxis xAxis = chart1.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        //left
        ValueFormatter custom = new MyValueFormatter("$");
        YAxis leftAxis = chart1.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setLabelCount(8,false);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setValueFormatter(custom);

        //right
        YAxis rightAxis = chart1.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8,false);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f);
        rightAxis.setValueFormatter(custom);

        //set legend
        Legend legend = chart1.getLegend();
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        legend.setDrawInside(false);
        legend.setForm(Legend.LegendForm.SQUARE);
        legend.setFormSize(9f);
        legend.setTextSize(11f);
        legend.setXEntrySpace(4f);

        //set xy Marker
        XYMarkerView markerView = new XYMarkerView(context,xAxisFormatter);
        markerView.setChartView(chart1);
        chart1.setMarker(markerView);

        //set seekbar
        seekBarX.setProgress(12);
        seekBarY.setProgress(50);
    }

    //BarChart ----------------------------------------------------------------------
    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
    //BarChart ----------------------------------------------------------------------

    //SeekBar ----------------------------------------------------------------------
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        tvXMax.setText(Integer.toString(seekBarX.getProgress()));
        tvYMax.setText(Integer.toString(seekBarY.getProgress()));

        setDataChart(seekBarX.getProgress(), seekBarY.getProgress());
        //setiap berubah data chartnya di referesh
        chart1.invalidate();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
    //SeekBar ----------------------------------------------------------------------

    //create dummy data for bar chart
    private void setDataChart(int count, int range) {
        float start = 1f;

        ArrayList<BarEntry> values = new ArrayList<>();

        for (int i = (int) start; i < start + count; i++) {
            float value = (float) Math.random() * (range + 1);

            if((Math.random() * 100) < 25){
                values.add(new BarEntry(i, value, getResources().getDrawable(R.drawable.circle_selected)));
            }else{
                values.add(new BarEntry(i, value));
            }
        }

        BarDataSet barDataSet;
        if(chart1.getData() != null && chart1.getData().getDataSetCount() > 0){
            barDataSet = (BarDataSet) chart1.getData().getDataSetByIndex(0);
            barDataSet.setValues(values);

            chart1.getData().notifyDataChanged();
            chart1.notifyDataSetChanged();
        }else {
            barDataSet = new BarDataSet(values,"The Year 2017");
            barDataSet.setDrawIcons(false);

            //bikin dummy data
            int startColor1 = ContextCompat.getColor(this, android.R.color.holo_orange_light);
            int startColor2 = ContextCompat.getColor(this, android.R.color.holo_blue_light);
            int startColor3 = ContextCompat.getColor(this, android.R.color.holo_orange_light);
            int startColor4 = ContextCompat.getColor(this, android.R.color.holo_green_light);
            int startColor5 = ContextCompat.getColor(this, android.R.color.holo_red_light);
            int endColor1 = ContextCompat.getColor(this, android.R.color.holo_blue_dark);
            int endColor2 = ContextCompat.getColor(this, android.R.color.holo_purple);
            int endColor3 = ContextCompat.getColor(this, android.R.color.holo_green_dark);
            int endColor4 = ContextCompat.getColor(this, android.R.color.holo_red_dark);
            int endColor5 = ContextCompat.getColor(this, android.R.color.holo_orange_dark);

            List<GradientColor> gradientColors = new ArrayList<>();
            gradientColors.add(new GradientColor(startColor1, endColor1));
            gradientColors.add(new GradientColor(startColor2, endColor2));
            gradientColors.add(new GradientColor(startColor3, endColor3));
            gradientColors.add(new GradientColor(startColor4, endColor4));
            gradientColors.add(new GradientColor(startColor5, endColor5));

            barDataSet.setGradientColors(gradientColors);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(barDataSet);

            //bikin dummy data

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.9f);
            chart1.setData(data);
        }
    }
}
