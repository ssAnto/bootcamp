package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;

import com.example.example.R;
import com.example.example.adapters.ImageSliderAdapter;
import com.example.example.fragment.ImageSliderFragment;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class ImageSliderActivity extends AppCompatActivity {
    private Context context = this;
    private ViewPager imageViewPager;
    private CircleIndicator circleIndicator;
    private String[] image ={
            "https://www.thisiscolossal.com/wp-content/uploads/2016/05/NatGeo_02.jpg"
            ,
            "https://cdn.theatlantic.com/assets/media/img/photo/2017/11/2017-national-geographic-nature-pho/n01_51110811034491-1/facebook.jpg?1509746037"
            ,
            "https://www.nationalgeographic.com/content/dam/photography/photos/000/945/94569.ngsversion.1467402814206.adapt.1900.1.jpg"
            ,
            "https://mymodernmet.com/wp/wp-content/uploads/2018/04/NG-TPOTY2018-WEEK1-007.jpg"
            ,
            "https://www.nationalgeographic.com/content/dam/news/2016/03/12/Your%20Shot%20Underwater/01_underwaterys.ngsversion.1457722039265.jpg"
            ,
            "https://i.huffpost.com/gen/929582/images/o-NATIONAL-GEOGRAPHIC-PHOTO-CONTEST-2012-facebook.jpg"
            ,
            "https://www.thisiscolossal.com/wp-content/uploads/2018/12/NG_2018PHOTOCONTEST_WILDLIFE_1STPLACE.jpg"
            ,
            "https://twistedsifter.files.wordpress.com/2016/09/prod-yourshot-1186811-8874942.jpg"
            ,
            "https://www.nationalgeographic.com/content/dam/photography/photos/000/895/89536.ngsversion.1467254292613.adapt.1900.1.jpg"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slider);

        //init
        imageViewPager= findViewById(R.id.imageViewPager);
        circleIndicator=findViewById(R.id.cicleIndicator);

        showImageSlider();

    }

    void  showImageSlider(){
        //arraylist semua Image
        List <String> urlImageString = new ArrayList<>();
        for (int i = 0; i <image.length ; i++) {
            urlImageString.add(image[i]);
        }

        //siapkan array list fragment ....
        List<ImageSliderFragment> sliderFragmentList = new ArrayList<>();
        for (int i = 0; i <image.length; i++) {
            ImageSliderFragment fragment = new ImageSliderFragment();
            fragment.setImageURL(image[i]);
            fragment.setImageUrls(urlImageString);
            fragment.setPosition(i);
            sliderFragmentList.add(fragment);
        }
        //panggil adapter
        ImageSliderAdapter adapter_image = new ImageSliderAdapter(getSupportFragmentManager(), sliderFragmentList);

        //set adapter ke viewPager
        imageViewPager.setAdapter(adapter_image);

        //set circleindicator
        circleIndicator.setViewPager(imageViewPager);
    }
}
