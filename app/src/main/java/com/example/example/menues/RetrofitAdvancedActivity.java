package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.example.R;
import com.example.example.models.create_user.CreateUserModel;
import com.example.example.models.patch_user.PatchUserModel;
import com.example.example.models.put_user.PutUserModel;
import com.example.example.retrofit_utilities.APIUtilities;
import com.example.example.retrofit_utilities.RequestAPIServices;
import com.example.example.utilities.Loading;
import com.example.example.utilities.Tools;
import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitAdvancedActivity extends AppCompatActivity {

    private Context context = this;
    private EditText inputName,inputJob,inputId;
    private Button postData,putData,patchData;
    private LinearLayout container;

    //retrofit api
    private RequestAPIServices apiServices = APIUtilities.getAPIServices();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit_advanced);

        //init
        inputName = findViewById(R.id.inputName);
        inputJob = findViewById(R.id.inputJob);
        inputId = findViewById(R.id.inputId);

        postData = findViewById(R.id.postData);
        putData = findViewById(R.id.putData);
        patchData = findViewById(R.id.patchData);

        container=findViewById(R.id.containerRetrofitAdvanced);

        postData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //parameter untuk input
                String name = inputName.getText().toString();
                String job = inputJob.getText().toString();
                postDataUser(name,job);
                Tools.hideKeyboard(context,getCurrentFocus().getWindowToken());
            }
        });

        putData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = inputName.getText().toString();
                String job = inputJob.getText().toString();
                int id = Integer.parseInt(inputId.getText().toString());
                putDataUser(name,job,id);
                Tools.hideKeyboard(context,getCurrentFocus().getWindowToken());
            }
        });

        patchData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = inputName.getText().toString();
                String job = inputJob.getText().toString();
                int id = Integer.parseInt(inputId.getText().toString());
                patchDataUser(name,job,id);
                Tools.hideKeyboard(context,getCurrentFocus().getWindowToken());
            }
        });

        //auto hide keyboard saat focus diluar edit text

    }

    void patchDataUser(String name, String job, int id){
        final ProgressDialog loading = Loading.customLoadingAnimation(context);
        loading.show();
        apiServices.patchUser(id,name,job).enqueue(new Callback<PatchUserModel>() {
            @Override
            public void onResponse(Call<PatchUserModel> call, Response<PatchUserModel> response) {
                if(response.code() == 200){
                    loading.dismiss();
                    //kalo berhasil
                    String massage =response.body().getName() +" At : "+response.body().getUpdatedAt();
                    Snackbar.make(container,massage,Snackbar.LENGTH_LONG).show();

                }else{
                    //kalo responnya gak 201
                    loading.dismiss();
                    Toast.makeText(context,"Ada Gangguan Jaringan"+response.code(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PatchUserModel> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(context,"Gagal Patch User "+t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    void postDataUser(String name, String job){
        final ProgressDialog loading = Loading.customLoadingAnimation(context);
        loading.show();
        apiServices.createUser(name,job).enqueue(new Callback<CreateUserModel>() {
            @Override
            public void onResponse(Call<CreateUserModel> call, Response<CreateUserModel> response) {

                if(response.code() == 201){
                    loading.dismiss();
                    //kalo berhasil
                    String massage =response.body().getName() +" At : "+response.body().getCreatedAt();
                    Snackbar.make(container,massage,Snackbar.LENGTH_LONG).show();

                }else{
                    //kalo responnya gak 201
                    loading.dismiss();
                    Toast.makeText(context,"Ada Gangguan Jaringan"+response.code(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CreateUserModel> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(context,"Gagal Post "+t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    void putDataUser(String name,String job, int id){
        final ProgressDialog loading = Loading.customLoadingAnimation(context);
        loading.show();

        apiServices.putUser(id,name,job).enqueue(new Callback<PutUserModel>() {
            @Override
            public void onResponse(Call<PutUserModel> call, Response<PutUserModel> response) {

                if(response.code() == 200){
                    loading.dismiss();
                    //kalo berhasil
                    String massage =" Success Update Data User At : "+response.body().getUpdatedAt();
                    Snackbar.make(container,massage,Snackbar.LENGTH_LONG).show();

                }else{
                    //kalo responnya gak 201
                    loading.dismiss();
                    Toast.makeText(context,"Ada Gangguan Jaringan"+response.code(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PutUserModel> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(context,"Gagal Put User "+t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

}
