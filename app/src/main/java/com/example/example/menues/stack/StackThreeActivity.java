package com.example.example.menues.stack;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.example.MainActivity;
import com.example.example.R;

public class StackThreeActivity extends AppCompatActivity {
    private Context context=this;
    private Button aaa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stack_three);

        aaa=findViewById(R.id.next);
        aaa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bbb = new Intent(context, MainActivity.class);
                bbb.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                bbb.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(bbb);
            }
        });
    }
}
