package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

import com.example.example.R;

public class WebViewActivity extends AppCompatActivity {
    private Context context = this;
    private WebView webView;
    private Button ok;
    private EditText url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        //init
        webView=findViewById(R.id.webView1);
        ok=findViewById(R.id.ok);
        url=findViewById(R.id.url);

        //web setting
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url_now = url.getText().toString();
                webView.loadUrl(url_now);
            }
        });
    }
}
