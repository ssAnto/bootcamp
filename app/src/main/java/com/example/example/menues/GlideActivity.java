package com.example.example.menues;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.example.R;
import com.squareup.picasso.Picasso;

public class GlideActivity extends AppCompatActivity {
    private Context context= this;
    private ImageView imageView;
    private Button buttonLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glide);

        //init
        buttonLoad = findViewById(R.id.button_load_image_glide);
        imageView = findViewById(R.id.preview_image_glide);
        buttonLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImageGlide();
            }
        });
    }

    void loadImageGlide(){
        String urlImage = "https://1.bp.blogspot.com/-saSjjW1UVvA/Wcttal9Pz4I/AAAAAAAAGho/Yw_QMsLDTAII-XuE_P-Gjj2Zw6XQQ5MSgCLcBGAs/s1600/sejarah-candi-prambanan-gambar-legenda.png";
        Glide.with(context)
                .load(urlImage)
                //.error(R.drawable.ic_broken)
                //.placeholder(R.drawable.ic_default_image_account)
                .into(imageView);
    }
}
