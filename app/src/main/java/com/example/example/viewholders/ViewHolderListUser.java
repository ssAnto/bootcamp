package com.example.example.viewholders;

import android.content.Context;
import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.example.R;
import com.example.example.models.list_user.Datum;
import com.squareup.picasso.Picasso;

public class ViewHolderListUser extends RecyclerView.ViewHolder {

    private TextView fullname,email,id;
    private ImageView avatar;
    public ViewHolderListUser(@NonNull View itemView) {
        super(itemView);

        avatar=itemView.findViewById(R.id.avatarRetrofit);
        email=itemView.findViewById(R.id.emailRetrofit);
        id=itemView.findViewById(R.id.idRetrofit);
        fullname=itemView.findViewById(R.id.fullnameRetrofit);
    }

    public void setModel(Datum dataUser, Context context){
        fullname.setText(dataUser.getFirstName()+" "+dataUser.getLastName());
        email.setText(dataUser.getEmail());
        id.setText(Integer.toString(dataUser.getId()));
        Glide.with(context)
                .load(dataUser.getAvatar())
                //.placeholder(R.drawable.ic_default_image_account)
                //.error(R.drawable.ic_broken)
                .into(avatar);
    }
}
