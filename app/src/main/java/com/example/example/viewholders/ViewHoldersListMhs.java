package com.example.example.viewholders;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.example.R;

import org.w3c.dom.Text;

public class ViewHoldersListMhs extends RecyclerView.ViewHolder {

    public CardView contentListMhs;
    private TextView namaMhs;
    private ImageView foto_profil;

    public ViewHoldersListMhs(@NonNull View itemView) {
        super(itemView);
        contentListMhs = itemView.findViewById(R.id.conten_listMhs);
        namaMhs = itemView.findViewById(R.id.namaMhs);
        foto_profil = itemView.findViewById(R.id.fotoProfilMhs);
    }

    @SuppressLint("ResourceAsColor")
    public void setNamaMhs(Context context, String nama, String PATH, int index){
        if(nama !=null){
            namaMhs.setText(nama);
            Glide.with(context)
                    .load(PATH)
                    .into(foto_profil);
        }

        //bedakan warna list
        if(index%2==0){
            contentListMhs.setBackgroundColor(Color.GRAY);
        }else{
            contentListMhs.setBackgroundColor(R.color.light_grey);
        }
    }
}
