package com.example.example.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.example.R;
import com.example.example.models.BukuModel;

public class ViewHolderListBuku extends RecyclerView.ViewHolder {

    private TextView judulBuku,penerbitBuku,pengarangBuku,kategoriBuku,stockBuku,hargaBuku;
    public ImageView btnEdit,btnDelete;
    public ViewHolderListBuku(@NonNull View itemView) {
        super(itemView);

        //init
        judulBuku = itemView.findViewById(R.id.judulBuku);
        penerbitBuku = itemView.findViewById(R.id.penerbitBuku);

        pengarangBuku = itemView.findViewById(R.id.pengarangBuku);
        kategoriBuku = itemView.findViewById(R.id.kategoriBuku);

        stockBuku = itemView.findViewById(R.id.stockBuku);
        hargaBuku = itemView.findViewById(R.id.hargaBuku);
        btnEdit = itemView.findViewById(R.id.btnEdit);
        btnDelete = itemView.findViewById(R.id.btnDelete);
    }

    public void setBuku (BukuModel buku){
        judulBuku.setText(buku.getJudul_buku());
        penerbitBuku.setText(buku.getPenerbit_buku());
        pengarangBuku.setText(buku.getPengarang_buku());
        stockBuku.setText(Integer.toString(buku.getStok()));
        hargaBuku.setText(Integer.toString(buku.getHarga()));
        kategoriBuku.setText(buku.getKategori_buku());
    }
}
