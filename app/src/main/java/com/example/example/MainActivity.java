package com.example.example;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.example.menues.AdvanceParsingActivity;
import com.example.example.menues.AirLocationActivity;
import com.example.example.menues.CameraActivity;
import com.example.example.menues.ChartActivity;
import com.example.example.menues.DigitalSignatureActivity;
import com.example.example.menues.EsafirmActivity;
import com.example.example.menues.GaleryActivity;
import com.example.example.menues.GlideActivity;
import com.example.example.menues.GoogleMapsAPIActivity;
import com.example.example.menues.ImageSliderActivity;
import com.example.example.menues.ListViewActivity;
import com.example.example.menues.LocationTrackingActivity;
import com.example.example.menues.MultiLevelParsingActivity;
import com.example.example.menues.PicasoActivity;
import com.example.example.menues.ROOM.RoomActivity;
import com.example.example.menues.RetrofitActivity;
import com.example.example.menues.RetrofitAdvancedActivity;
import com.example.example.menues.TabbedMenuActivity;
import com.example.example.menues.VolleyActivity;
import com.example.example.menues.VolleyJSONParsingActivity;
import com.example.example.menues.WebViewActivity;
import com.example.example.menues.mahasiswa.DaftarMahasiswaActivity;
import com.example.example.menues.perpustakaan.PerpustakaanActivity;
import com.example.example.menues.stack.StackOneActivity;
import com.example.example.utilities.Konstanta;
import com.example.example.utilities.SessionManager;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private Context context = this;

    private TextView lbluname;
    private Button btnlogout;
    private int[] id_menu={
            R.id.menu1,
            R.id.menu2,
            R.id.menu3,
            R.id.menu4,
            R.id.menu5,
            R.id.menu6,
            R.id.menu7,
            R.id.menu8,
            R.id.menu9,
            R.id.menu10,
            R.id.menu11,
            R.id.menu12,
            R.id.menu13,
            R.id.menu14,
            R.id.menu15,
            R.id.menu16,
            R.id.menu17,
            R.id.menu18,
            R.id.menu19,
            R.id.menu20,
            R.id.menu21,
            R.id.menu22,
            R.id.menu23,
            R.id.menu24,
            R.id.menu25,
            R.id.menu26
    };
    private CardView[] menu = new CardView[id_menu.length];
    private int counter=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init
        lbluname = (TextView)findViewById(R.id.lbluname);
        btnlogout = (Button)findViewById(R.id.btnLogout);
        for (int i = 0; i < id_menu.length; i++) {
            menu[i]=(CardView)findViewById(id_menu[i]);
        }
        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutApp();
            }
        });

        menu[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ListViewActivity.class);
                startActivity(intent);
            }
        });
        menu[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VolleyActivity.class);
                startActivity(intent);
            }
        });
        menu[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VolleyJSONParsingActivity.class);
                startActivity(intent);
            }
        });
        menu[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PicasoActivity.class);
                startActivity(intent);
            }
        });
        menu[4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GlideActivity.class);
                startActivity(intent);
            }
        });
        menu[5].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdvanceParsingActivity.class);
                startActivity(intent);
            }
        });
        menu[6].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MultiLevelParsingActivity.class);
                startActivity(intent);
            }
        });
        menu[7].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RetrofitActivity.class);
                startActivity(intent);
            }
        });
        menu[8].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RetrofitAdvancedActivity.class);
                startActivity(intent);
            }
        });
        menu[9].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TabbedMenuActivity.class);
                startActivity(intent);
            }
        });
        menu[10].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImageSliderActivity.class);
                startActivity(intent);
            }
        });
        menu[11].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CameraActivity.class);
                startActivity(intent);
            }
        });
        menu[12].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GaleryActivity.class);
                startActivity(intent);
            }
        });
        menu[13].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EsafirmActivity.class);
                startActivity(intent);
            }
        });
        menu[14].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DaftarMahasiswaActivity.class);
                startActivity(intent);
            }
        });
        menu[15].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PerpustakaanActivity.class);
                startActivity(intent);
            }
        });

        menu[16].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LocationTrackingActivity.class);
                startActivity(intent);
            }
        });
        menu[17].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AirLocationActivity.class);
                startActivity(intent);
            }
        });
        menu[18].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GoogleMapsAPIActivity.class);
                startActivity(intent);
            }
        });
        menu[19].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GoogleMapsAPIActivity.class);
                startActivity(intent);
            }
        });
        menu[20].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChartActivity.class);
                startActivity(intent);
            }
        });
        menu[21].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RoomActivity.class);
                startActivity(intent);
            }
        });
        menu[22].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StackOneActivity.class);
                startActivity(intent);
            }
        });
        menu[23].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DigitalSignatureActivity.class);
                startActivity(intent);
            }
        });
        menu[24].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebViewActivity.class);
                startActivity(intent);
            }
        });
        menu[25].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebViewActivity.class);
                startActivity(intent);
            }
        });
        //getvalue dari intent extra LoginActivity
        //cekUnameExtras();
        //ambil uname dari SessionManager
        cekUnameSessionManager();
    }

    void cekUnameExtras(){
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null)
        {
            String uname = bundle.getString(Konstanta.USERNAME);
            uname = "Welcome, Mr."+uname;
            lbluname.setText(uname);
        }
    }

    void cekUnameSessionManager(){
        String uname = SessionManager.getUnameSessionManager(context);
        lbluname.setText(uname);
    }

    void logoutApp(){
        SessionManager.clearData(context);

        Intent intent = new Intent(context, LoginForm.class);
        startActivity(intent);

        finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

//        //cara pertama !!!
//        AlertDialog.Builder confirmation = new AlertDialog.Builder(context);
//        confirmation.setMessage("Anda Yakin Mau Keluar ?")
//                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        finish();
//                    }
//                })
//                .setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                })
//                .setCancelable(false);
//        confirmation.show();
//
        //cara kedua !!!
        if(counter<1) {
            Toast.makeText(context, "Tekan Back Sekali Lagi untuk Keluar", Toast.LENGTH_SHORT).show();
            counter++;
            countDownReset();
        }else{
            finish();
        }
    }

    void countDownReset(){
        TimerTask task = new TimerTask(){
            @Override
            public void run() {
                counter=0;
            }
        };
        Timer timer = new Timer();
        timer.schedule(task,1000);
    }

    //untuk mengatasi kelemahan pertama ..
    @Override
    protected void onResume() {
        super.onResume();
        counter=0;
    }
}
