package com.example.example.database_utilities;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DatabasePerpustakaanHelper extends SQLiteOpenHelper {
    private Context context;
    private static String DATABASE_NAME = "database_perpustakaan.db";
    private static int DATABASE_VERSION = 1;
    private static String DATABASE_PATH="";

    public DatabasePerpustakaanHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);

        this.context=context;
    }

    // Cek database ada atau tidak ....
    private boolean checkDatabase(){
        SQLiteDatabase db = null;

        File dbFile = context.getDatabasePath(DATABASE_NAME);
        if(!dbFile.getParentFile().exists() && !dbFile.getParentFile().mkdir()){
            //throw IOException("Couldn't make database directory !");
            return  false;
        }else{
            DATABASE_PATH=dbFile.getPath();
            try {
                db = SQLiteDatabase.openDatabase(DATABASE_PATH,
                        null, SQLiteDatabase.OPEN_READONLY);
            }catch (SQLException ex){
                System.out.println("checkDatabase : "+ex.getMessage());
            }finally {
                if(db !=null ){
                    db.close();
                }
            }

            if(db==null){
                return false;
            }else {
                return  true;
            }
        }
    }

    //untuk import database
    private void importDatabase() throws IOException{
        //buka file.db
        InputStream inputStream = context.getAssets().open(DATABASE_NAME);

        //siapkan path untuk simpan hasil import
        String outputFilePath = DATABASE_PATH;

        //write file
        OutputStream outputStream = new FileOutputStream(outputFilePath);

        byte[] buffer = new byte[1024];
        int length;
        while((length=inputStream.read(buffer)) > 0){
            outputStream.write(buffer,0,length);
        }
        //tutup semua koneksi
        outputStream.flush();
        outputStream.close();
        inputStream.close();
        System.out.println("Success Import Database !!!");
    }

    //method untuk buat database dari file import
    public boolean createDatabaseFromImportedFile() throws IOException{
        boolean result ;
        if(checkDatabase()){
            System.out.println("Database sudah ADA !!!!");
            result  =true;
        }else {
            //database belum ada
            importDatabase();
            result = false;
        }
        return  result;
    }

    //method delete database
    private void deleteDatabase(){
        File file = new File(DATABASE_PATH);
        if(file.exists()){
            //hapus file
            file.delete();
            System.out.println("Database dihapus !!!");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //tidak dilakukan apa2 karena pakai db dari db brosers sqlite
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion<newVersion){
            deleteDatabase();
        }
    }
}
