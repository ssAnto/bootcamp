package com.example.example.database_utilities;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseMahasiswaHelper extends SQLiteOpenHelper {

    final static String DATABASE_NAME = "database_mahasiswa.db";
    final static int DATABASE_VERSION = 1;

    public DatabaseMahasiswaHelper(@Nullable Context context) {
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        //Create Table
        String query = "CREATE TABLE '"+DatabaseConstanta.TABEL_BIODATA+"' ('"+
                DatabaseConstanta.ID+"' INTEGER PRIMARY KEY AUTOINCREMENT, '"+//0
                DatabaseConstanta.NIM+"' TEXT, '"+           //1
                DatabaseConstanta.NAMA_LENGKAP+"' TEXT, '"+  //2
                DatabaseConstanta.GENDER+"' TEXT, '"+        //3
                DatabaseConstanta.TANGGAL_LAHIR+"' TEXT, '"+ //4
                DatabaseConstanta.ALAMAT+"' TEXT, '"+        //5
                DatabaseConstanta.JURUSAN+"' TEXT, '"+       //6
                DatabaseConstanta.PATH_PHOTO+"' TEXT)";      //7

        //execute query
        db.execSQL(query);

        String queryinsert = "INSERT INTO "+DatabaseConstanta.TABEL_BIODATA+"("+DatabaseConstanta.NIM+","+DatabaseConstanta.NAMA_LENGKAP+","+DatabaseConstanta.TANGGAL_LAHIR+","+DatabaseConstanta.GENDER+","+DatabaseConstanta.ALAMAT+","+DatabaseConstanta.JURUSAN+","+DatabaseConstanta.PATH_PHOTO+") VALUES" +
                "('20190445','Listianto Raharjo','06/06/1996','Laki-laki','Pondok Pinang, Jakarta Selatan','HUKUM','')";
        db.execSQL(queryinsert);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
