package com.example.example.database_utilities;

public class DatabaseConstanta {
    //database konstanta DBMhs
    public static String TABEL_BIODATA = "biodata";
    public static String ID = "id";
    public static String NIM = "nim";
    public static String NAMA_LENGKAP = "nama_lengkap";
    public static String GENDER = "gender";
    public static String TANGGAL_LAHIR = "tanggal_lahir";
    public static String ALAMAT = "alamat";
    public static String JURUSAN = "jurusan";
    public static String PATH_PHOTO = "path_photo";

    //database konstanta DBPerpustakaan
    public static String TABLE_STOK_BUKU="stock_buku";
    public static String ID_BUKU="id";
    public static String JUDUL_BUKU="judul_buku";
    public static String KATEGORI_BUKU="kategori_buku";
    public static String PENGARANG_BUKU="pengarang_buku";
    public static String PENERBIT_BUKU="penerbit_buku";
    public static String HARGA="harga";
    public static String STOCK="stock";
}
