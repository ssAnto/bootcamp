package com.example.example.database_utilities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;

import com.example.example.models.BukuModel;

import java.util.ArrayList;

public class DatabasePerpustakaanQuerryHelper {

    private DatabasePerpustakaanHelper databasePerpustakaanHelper;


    public DatabasePerpustakaanQuerryHelper(DatabasePerpustakaanHelper databasePerpustakaanHelper) {
        this.databasePerpustakaanHelper=databasePerpustakaanHelper;
    }

    private Cursor readSemuaBuku(){
        SQLiteDatabase db = databasePerpustakaanHelper.getReadableDatabase();
        String queryRead = "SELECT * FROM "+DatabaseConstanta.TABLE_STOK_BUKU;
        Cursor cursor = db.rawQuery(queryRead,null);
        System.out.println(queryRead);
        System.out.println(cursor.getCount());
        return cursor;
    }

    private ArrayList<BukuModel> konversiCursorkeArrayList (Cursor cursor){
        ArrayList<BukuModel> listBuku = new ArrayList<>();

        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);

            BukuModel bukuModel = new BukuModel();
            bukuModel.setId(cursor.getInt(0));
            bukuModel.setJudul_buku(cursor.getString(1));
            bukuModel.setKategori_buku(cursor.getString(2));
            bukuModel.setPengarang_buku(cursor.getString(3));
            bukuModel.setPenerbit_buku(cursor.getString(4));
            bukuModel.setHarga(cursor.getInt(5));
            bukuModel.setStok(cursor.getInt(6));

            listBuku.add(bukuModel);
        }
        return listBuku;
    }

    private  Cursor searchBuku(String keyword){
        SQLiteDatabase db = databasePerpustakaanHelper.getReadableDatabase();

        String querryRead = "SELECT * FROM "+DatabaseConstanta.TABLE_STOK_BUKU+"" +
                " WHERE " + DatabaseConstanta.JUDUL_BUKU +
                " LIKE " +
                "'%" + keyword+
                "%' OR " + DatabaseConstanta.PENGARANG_BUKU +
                " LIKE '" +
                "%" +keyword+
                "%' OR " + DatabaseConstanta.PENERBIT_BUKU +
                " LIKE '" +
                "%" + keyword+
                "%'";
        Cursor cursor = db.rawQuery(querryRead,null);
        System.out.println(querryRead);
        System.out.println(cursor.getCount());
        return cursor;
    }

    public ArrayList<BukuModel> getAllListBuku(){
        ArrayList<BukuModel> bukuModels = new ArrayList<>();
        Cursor cursor= readSemuaBuku();
        if(cursor.getCount()>0){
            bukuModels = konversiCursorkeArrayList(cursor);
        }
        return  bukuModels;
    }

    //fungsi search buku

    public  ArrayList<BukuModel> searchListBuku (String keyword){
        ArrayList<BukuModel> bukuModels = new ArrayList<>();
        Cursor cursor= searchBuku(keyword);
        if(cursor.getCount()>0){
            bukuModels = konversiCursorkeArrayList(cursor);
        }
        return  bukuModels;
    }

    //method untuk insert buku baru ....
    public void addBuku(BukuModel buku){
        SQLiteDatabase db = databasePerpustakaanHelper.getWritableDatabase();

        ContentValues content = new ContentValues();
        content.put(DatabaseConstanta.JUDUL_BUKU,buku.getJudul_buku());
        content.put(DatabaseConstanta.KATEGORI_BUKU,buku.getKategori_buku());
        content.put(DatabaseConstanta.PENGARANG_BUKU,buku.getPengarang_buku());
        content.put(DatabaseConstanta.PENERBIT_BUKU,buku.getPenerbit_buku());
        content.put(DatabaseConstanta.HARGA,buku.getHarga());
        content.put(DatabaseConstanta.STOCK,buku.getStok());

        db.insert(DatabaseConstanta.TABLE_STOK_BUKU,null,content);

    }

    //method untuk delete buku
    public void delete(String id){
        SQLiteDatabase db = databasePerpustakaanHelper.getWritableDatabase();

        String querryDelete = "DELETE FROM "+DatabaseConstanta.TABLE_STOK_BUKU +
                " WHERE "+DatabaseConstanta.ID+" = "+id;

        db.execSQL(querryDelete,null);
    }

    //method untuk get buku by id
    public BukuModel getBukumodel(String id){
        SQLiteDatabase db = databasePerpustakaanHelper.getWritableDatabase();

        BukuModel bukuModel = new BukuModel();
        String query = "SELECT * FROM "+DatabaseConstanta.TABLE_STOK_BUKU +
                " WHERE "+DatabaseConstanta.ID+" = "+id;
        Cursor cursor = db.rawQuery(query,null);

        bukuModel=konversiCursorkeArrayList(cursor).get(0);

        return bukuModel;
    }
}
