package com.example.example.retrofit_utilities;

import com.example.example.models.create_user.CreateUserModel;
import com.example.example.models.list_user.ListUserModel;
import com.example.example.models.login.LoginUserModel;
import com.example.example.models.patch_user.PatchUserModel;
import com.example.example.models.put_user.PutUserModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RequestAPIServices {
    //deklarasikan semua method pada api yg akan digunakan

    //Panggil List User
    @GET("users")
    Call<ListUserModel> getListUser(@Query("page") int page);

    //kirim data user
    @FormUrlEncoded
    @POST("users")
    Call<CreateUserModel> createUser(@Field("name") String name, @Field("job") String job);

    //put user
    @FormUrlEncoded
    @PUT("users/{id}")
    Call<PutUserModel> putUser(@Path("id") int id, @Field("name") String name, @Field("job") String job);

    //patch user
    @FormUrlEncoded
    @PATCH("user/{id}")
    Call<PatchUserModel> patchUser(@Path("id") int id,@Field("name") String name, @Field("job") String job);


}
