package com.example.example.retrofit_utilities;

import android.app.DownloadManager;

public class APIUtilities {
    private static String BASE_URL_API = "https://reqres.in/api/";
    public static RequestAPIServices getAPIServices(){
        return RetrofitClient.getClient(BASE_URL_API).create(RequestAPIServices.class);

    }
}
