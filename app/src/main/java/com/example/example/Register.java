package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.service.autofill.RegexValidator;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.example.utilities.Konstanta;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Register extends AppCompatActivity {
    private Context context = this;

    private EditText nama,alamat,umur,nomor_telepon,alamat_email;
    private RadioGroup gender_group;
    private Spinner jenis_pekerjaan,gelar_pendidikan,hobi;
    private CheckBox agree;
    private TextView kalender;
    private Button regis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        jenis_pekerjaan=(Spinner) findViewById(R.id.jenis_pekerjaan);
        gelar_pendidikan=(Spinner) findViewById(R.id.gelar_pendidikan);
        hobi = (Spinner)findViewById(R.id.hobi);
        kalender=(TextView) findViewById(R.id.tanggal_lahir);
        agree=(CheckBox)findViewById(R.id.agreement_check);
        regis = (Button)findViewById(R.id.button_register);
        nama = findViewById(R.id.nama_lengkap);
        alamat = findViewById(R.id.alamat);
        umur = findViewById(R.id.umur);
        nomor_telepon =findViewById(R.id.telepon);
        alamat_email = findViewById(R.id.email);
        gender_group = findViewById(R.id.gender);

        setAllSpinnerData();
        cekAgreement();

        kalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pilihTanggalLahir();
            }
        });
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validasiInput();
            }
        });

    }

    void validasiInput(){
        String nama_tmp = nama.getText().toString().trim();
        String alamat_tmp = alamat.getText().toString().trim();
        String telepon_tmp = nomor_telepon.getText().toString().trim();
        String email=alamat_email.getText().toString().trim();
        String regex = "^[A-Za-z0-9]+@(.+)";
        Pattern pattern = Pattern.compile(regex);

        if(nama_tmp.length()==0){
            Toast.makeText(context,"Nama Harus Diisi !!!!",Toast.LENGTH_SHORT).show();
        }else if(alamat_tmp.length()==0){
            Toast.makeText(context,"Alamat Harus Diisi !!!!",Toast.LENGTH_SHORT).show();
        }else if(umur.getText().toString().isEmpty()){
            Toast.makeText(context,"Usia Harus Diisi !!!!",Toast.LENGTH_SHORT).show();
        }else if(telepon_tmp.length()==0){
            Toast.makeText(context,"Nomor Telepon Harus Diisi !!!!",Toast.LENGTH_SHORT).show();
        }else if(pattern.matcher(email).matches()){
            Toast.makeText(context,"Email Anda Salah !!!!",Toast.LENGTH_SHORT).show();
//        }else if(jenis_pekerjaan.getChildAt()==0){
//            Toast.makeText(context,"Jenis Pekerjaan Harus Diisi !!!!",Toast.LENGTH_SHORT).show();
//        }else if(gelar_pendidikan.getChildAt()==0){
//            Toast.makeText(context,"Gelar Pendidikan Harus Diisi !!!!",Toast.LENGTH_SHORT).show();
//        }else if(hobi.getChildAt()==0){
//            Toast.makeText(context,"Hobi Harus Diisi !!!!",Toast.LENGTH_SHORT).show();
        }else if(kalender.getText().toString().isEmpty()){
            Toast.makeText(context,"Pilih Tanggal Lahir !!!!",Toast.LENGTH_SHORT).show();
        }else{
            finish();
        }
    }

    void pilihTanggalLahir(){
        Calendar today = Calendar.getInstance();

        int yearNow = today.get(Calendar.YEAR);
        int monthNow = today.get(Calendar.MONTH);
        int dayNow = today.get(Calendar.DATE);

        DatePickerDialog datePicker = new DatePickerDialog(context,
                R.style.CustomDatePicker,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar selectedDate = Calendar.getInstance();
                        selectedDate.set(year,month,dayOfMonth);

                        //konversi dari kalender menjadi string
                        SimpleDateFormat formter = new SimpleDateFormat("dd/MM/yyyy");
                        String tanggal = formter.format(selectedDate.getTime());

                        //set ke tampilan
                        kalender.setText(tanggal);
                    }
                }, yearNow,monthNow,dayNow);

        datePicker.show();
    }

    void setAllSpinnerData(){
        //1 data untuk jenis spiner pekerjaan -- cara 1 simpan array string di constanta
        ArrayAdapter<String> adapterJenisPekerjaan =
                new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,
                        Konstanta.ARRAY_PEKERJAAN);
        jenis_pekerjaan.setAdapter(adapterJenisPekerjaan);

        //2 data untuk spinner gelar pendidikan - cara 2
        ArrayAdapter<CharSequence> adapterGelarPendidikan =
                ArrayAdapter.createFromResource(context,
                        R.array.gelar,android.R.layout.simple_spinner_item);
        gelar_pendidikan.setAdapter(adapterGelarPendidikan);


        ArrayAdapter<String> adapterHobi =
                new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,
                        Konstanta.ARRAY_HOBY);
        hobi.setAdapter(adapterHobi);

    }
    void cekAgreement(){
        regis.setEnabled(false);
        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regis.setEnabled(agree.isChecked());
            }
        });
    }
}
