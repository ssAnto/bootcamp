package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.example.utilities.Konstanta;
import com.example.example.utilities.SessionManager;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    private Context context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //untuk hilangkan toolbar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                             WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_splash);
        timerSementara();

    }
    void pindahMainActivity(){
        //Inten untuk pindah antar screen
        Intent intent = new Intent(context,MainActivity.class);
        startActivity(intent);
        finish();
    }

    void timerSementara(){
        //timer task
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if(SessionManager.cekLoginFlag(context)){
                    pindahMainActivity();
                }else{
                    pindahLoginActivity();
                }
            }
        };
        //timer
        Timer timer = new Timer();
        timer.schedule(task, Konstanta.DELAY_PLASH_SCREEN);
    }

    void pindahLoginActivity(){
        //Inten untuk pindah antar screen
        Intent intent = new Intent(context,LoginForm.class);
        startActivity(intent);
        finish();
    }


}
