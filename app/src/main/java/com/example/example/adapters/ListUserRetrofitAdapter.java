package com.example.example.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.example.R;
import com.example.example.models.list_user.Datum;
import com.example.example.viewholders.ViewHolderListUser;

import java.util.List;

public class ListUserRetrofitAdapter extends RecyclerView.Adapter<ViewHolderListUser> {
    private Context context;
    private List<Datum> listUser;

    public ListUserRetrofitAdapter(Context context, List<Datum> listUser) {
        this.context=context;
        this.listUser=listUser;
    }

    @NonNull
    @Override
    public ViewHolderListUser onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customLayout = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_list_layout_retrofit,
                parent,
                false
        );

        return new ViewHolderListUser(customLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderListUser holder, int position) {
        Datum dataUser = listUser.get(position);
        holder.setModel(dataUser,context);
    }


    @Override
    public int getItemCount() {
        return listUser.size();
    }
}
