package com.example.example.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.example.R;
import com.example.example.database_utilities.DatabasePerpustakaanHelper;
import com.example.example.database_utilities.DatabasePerpustakaanQuerryHelper;
import com.example.example.menues.perpustakaan.ListBukuActivity;
import com.example.example.models.BukuModel;
import com.example.example.viewholders.ViewHolderListBuku;


import java.util.ArrayList;

public class ListBukuAdapter extends RecyclerView.Adapter<ViewHolderListBuku> {

    private Context context;
    private ArrayList<BukuModel> listBukuModels;
    private DatabasePerpustakaanHelper databasePerpustakaanHelper = new DatabasePerpustakaanHelper(context);
    private DatabasePerpustakaanQuerryHelper databasePerpustakaanQuerryHelper =
            new DatabasePerpustakaanQuerryHelper(databasePerpustakaanHelper);

    public ListBukuAdapter(Context context, ArrayList<BukuModel> listBukuModels) {
        this.context = context;
        this.listBukuModels = listBukuModels;
    }


    @NonNull
    @Override
    public ViewHolderListBuku onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View customView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_list_buku_layout,parent,false);

        return new ViewHolderListBuku(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderListBuku holder, final int position) {
        holder.setBuku(listBukuModels.get(position));
        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder konfirmasi = new AlertDialog.Builder(context);
                konfirmasi.setMessage("Anda Yakin Mau Delete ?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //fungsi delete
                                databasePerpustakaanQuerryHelper.delete(Integer.toString(listBukuModels.get(position).getId()));

                                //refresh list
                                ListBukuActivity activity = (ListBukuActivity) context;
                                activity.tampilkanSemuaBuku();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setCancelable(false);
                konfirmasi.create().show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listBukuModels.size();
    }
}
