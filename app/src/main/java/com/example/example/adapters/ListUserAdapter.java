package com.example.example.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.example.example.R;
import com.example.example.models.UserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListUserAdapter extends BaseAdapter {
    private  Context context;
    private ArrayList<UserModel> userModels;

    public ListUserAdapter(Context context, ArrayList<UserModel> userModels) {
        this.context = context;
        this.userModels = userModels;
    }

    @Override
    public int getCount() {
        return userModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        final ViewHolderListUser holder;

        if(convertView==null){
            holder=new ViewHolderListUser();
            convertView = inflater.inflate(R.layout.list_user_layout,null);

            //init element
            holder.avatarUser=convertView.findViewById(R.id.avatarUser);
            holder.fullname = convertView.findViewById(R.id.fullname);
            holder.email = convertView.findViewById(R.id.email);
            holder.idUser = convertView.findViewById(R.id.idUser);
        }else{
            holder=(ViewHolderListUser) convertView.getTag();
        }

        String fullname = userModels.get(position).getFirst_name()+ " "+userModels.get(position).getLast_name();
        holder.fullname.setText(fullname);
        holder.email.setText(userModels.get(position).getEmail());
        holder.idUser.setText(Integer.toString(userModels.get(position).getId()));

        String urlAvatar = userModels.get(position).getAvatar();
        Picasso.get()
                .load(urlAvatar)
                .placeholder(R.drawable.ic_default_image_account)
                .error(R.drawable.ic_broken)
                .into(holder.avatarUser);
//
//        ImageRequest avatarRequest = new ImageRequest(
//                urlAvatar,
//                new Response.Listener<Bitmap>() {
//                    @Override
//                    public void onResponse(Bitmap response) {
//                        holder.avatarUser.setImageBitmap(response);
//                    }
//                },
//                0,
//                0,
//                ImageView.ScaleType.FIT_CENTER,
//                Bitmap.Config.ARGB_8888,
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        System.out.println("Error : "+error.getMessage());
//                    }
//                }
//        );
//        Volley.newRequestQueue(context).add(avatarRequest);

        return convertView;
    }

    //ViewHolder
    static class ViewHolderListUser{
        private ImageView avatarUser;
        private TextView fullname, email,idUser;
    }
}
