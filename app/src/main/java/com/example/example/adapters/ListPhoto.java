package com.example.example.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.example.example.R;
import com.example.example.menues.DetailPhotoActivity;
import com.example.example.models.PhotoModel;
import com.example.example.utilities.TemporaryData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListPhoto extends BaseAdapter {

    private Context context;
    private ArrayList<PhotoModel> photoModels;

    public ListPhoto(Context context,ArrayList<PhotoModel> photoModels) {
        this.context=context;
        this.photoModels=photoModels;
    }

    @Override
    public int getCount() {
        return photoModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        final ViewHolderListPhoto holder;

        if(convertView==null){
            holder=new ViewHolderListPhoto();
            convertView = inflater.inflate(R.layout.list_photo,null);

            //init element
            holder.title = convertView.findViewById(R.id.title);
            holder.albumId = convertView.findViewById(R.id.id_image);
            holder.gambar = convertView.findViewById(R.id.thumnail);

        }else{
            holder=(ListPhoto.ViewHolderListPhoto) convertView.getTag();
        }

        holder.title.setText(photoModels.get(position).getTitle());
        holder.albumId.setText(Integer.toString(photoModels.get(position).getAlbungId()));
        String urlthump = photoModels.get(position).getThumbnailUrl();
        Glide.with(context)
                .load(urlthump)
                //.placeholder(R.drawable.ic_default_image_account)
                //.error(R.drawable.ic_broken)
                .into(holder.gambar);

        holder.gambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TemporaryData.setPhotoModel(photoModels.get(position));
                Intent intent= new Intent(context, DetailPhotoActivity.class);
                context.startActivities(new Intent[]{intent});
            }
        });



        return convertView;
    }

    class ViewHolderListPhoto{
        private ImageView gambar;
        private TextView title, albumId;
    }
}
