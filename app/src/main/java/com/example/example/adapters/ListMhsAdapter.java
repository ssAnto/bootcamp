package com.example.example.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.example.R;
import com.example.example.database_utilities.DatabaseConstanta;
import com.example.example.database_utilities.DatabaseMahasiswaHelper;
import com.example.example.menues.mahasiswa.DaftarMahasiswaActivity;
import com.example.example.menues.mahasiswa.DetailMahasiswaActivity;
import com.example.example.menues.mahasiswa.EditDataSiswaActivity;
import com.example.example.viewholders.ViewHoldersListMhs;

import java.util.List;

import retrofit2.http.PATCH;

public class ListMhsAdapter extends RecyclerView.Adapter<ViewHoldersListMhs> {

    private  Context context;
    private  List<String> namaMhs;
    private  List<Integer> idMhs;
    private  List<String> PATH;


    public ListMhsAdapter(Context context, List<String> namaMhs, List<Integer> idMhs, List<String> PATH) {
        this.context=context;
        this.namaMhs=namaMhs;
        this.idMhs=idMhs;
        this.PATH=PATH;
    }

    @NonNull
    @Override
    public ViewHoldersListMhs onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_list_mahasiswa, parent,false);

        return new ViewHoldersListMhs(myView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHoldersListMhs holder, final int position) {
        holder.setNamaMhs(context,namaMhs.get(position), PATH.get(position),idMhs.get(position));
        holder.contentListMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int id = idMhs.get(position);

                //dialog pilihan aksi
                CharSequence[] itemPilihan = {
                        "Lihat Detail Mahasiswa",
                        "Edit Data Mahasiswa",
                        "Delete Data Mahasiswa"
                };

                AlertDialog.Builder optionChooser = new AlertDialog.Builder(context);
                optionChooser.setTitle("Pilih Aksi ["+id+"]");
                optionChooser.setItems(itemPilihan, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent;
                        switch (which){
                            case 0 : //lihat detail
                                intent = new Intent(context, DetailMahasiswaActivity.class);
                                intent.putExtra(DatabaseConstanta.ID,id);
                                context.startActivity(intent);
                                break;
                            case 1 : //Edit data MHS
                                intent = new Intent(context, EditDataSiswaActivity.class);
                                intent.putExtra(DatabaseConstanta.ID,id);
                                context.startActivity(intent);
                                break;
                            case 2://delete data
                                AlertDialog.Builder konfirmasiDelete = new AlertDialog.Builder(context);
                                konfirmasiDelete.setMessage("Anda yakin mau menghapus data ini ?")
                                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                deleteDataMhs(id);
                                                //untuk panggil si refreshTampilan dari activitu daftar mhs;
                                                DaftarMahasiswaActivity activity = (DaftarMahasiswaActivity)context;
                                                activity.refreshTampilan();
                                            }
                                        })
                                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        })
                                        .setCancelable(false);
                                konfirmasiDelete.create().show();
                                break;
                        }
                    }
                });
                optionChooser.create().show();
            }
        });
    }

    void deleteDataMhs(int id){
        //delete data berdasarkan id
        DatabaseMahasiswaHelper databaseMahasiswaHelper = new DatabaseMahasiswaHelper(context);
        SQLiteDatabase db = databaseMahasiswaHelper.getWritableDatabase();

        String query_delete = "DELETE FROM "+DatabaseConstanta.TABEL_BIODATA+"" +
                " WHERE "+DatabaseConstanta.ID+" = "+id;
        System.out.println(query_delete);

        db.execSQL(query_delete);
    }

    @Override
    public int getItemCount() {
        return namaMhs.size();
    }
}
