package com.example.example.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.example.fragment.ImageSliderFragment;

import java.util.List;

public class ImageSliderAdapter extends FragmentPagerAdapter {

    private  List<ImageSliderFragment> imageSliderFragments;
    public ImageSliderAdapter(@NonNull FragmentManager fm, List<ImageSliderFragment> imageSliderFragments) {
        super(fm);
        this.imageSliderFragments=imageSliderFragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return imageSliderFragments.get(position);
    }

    @Override
    public int getCount() {
        return imageSliderFragments.size();
    }
}
