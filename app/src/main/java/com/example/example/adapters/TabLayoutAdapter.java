package com.example.example.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.example.R;
import com.example.example.fragment.FragmentDua;
import com.example.example.fragment.FragmentSatu;
import com.example.example.fragment.FragmentTiga;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class TabLayoutAdapter extends FragmentPagerAdapter {

    private TabLayout t;
    private Context context;
    public TabLayoutAdapter(Context context, FragmentManager fm, TabLayout t) {
        super(fm);
        this.t=t;
        this.context=context;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {

        t.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Drawable d = context.getDrawable(R.drawable.ic_broken);
                PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(Color.BLUE,
                        PorterDuff.Mode.SRC_ATOP);

                d.setColorFilter(porterDuffColorFilter);
                tab.setIcon(d);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Drawable d = context.getDrawable(R.drawable.ic_broken);
                PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(Color.GRAY,
                        PorterDuff.Mode.SRC_ATOP);

                d.setColorFilter(porterDuffColorFilter);
                tab.setIcon(d);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        if(position==0){
            t.getTabAt(position).setIcon(R.drawable.ic_broken);
            return new FragmentSatu();
        }else if(position==1){
            t.getTabAt(position).setIcon(R.drawable.ic_broken);
            return new FragmentDua();
        }else if(position==2){
            t.getTabAt(position).setIcon(R.drawable.ic_broken);
            return new FragmentTiga();
        }else{
            t.getTabAt(position).setIcon(R.drawable.ic_broken);
            return null;
        }

    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
//        switch (position){
//            case 0 : return "Tab ke 1";
//            case 1 : return "Tab ke 2";
//            case 2 : return "Tab ke 3";
//            default: return "";
//        }
        return null;
    }
}
