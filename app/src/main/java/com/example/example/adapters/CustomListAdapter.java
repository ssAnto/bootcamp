package com.example.example.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.example.R;

import org.w3c.dom.Text;

import static com.example.example.R.color.alice_blue;

public class CustomListAdapter extends BaseAdapter {
    private Context context;
    private String[] MERK_MOBIL;
    private String[] DESKRIPSI_MOBIL;
    private int[] ICON_MOBIL;

    public CustomListAdapter(Context context,
                             String[] MERK_MOBIL,
                             String[] DESKRIPSI_MOBIL,
                             int[] ICON_MOBIL){
        this.context=context;
        this.MERK_MOBIL=MERK_MOBIL;
        this.DESKRIPSI_MOBIL=DESKRIPSI_MOBIL;
        this.ICON_MOBIL=ICON_MOBIL;
    }

    @Override
    public int getCount(){
        return MERK_MOBIL.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.custom_list_layout, null);

            //init emelen custom list
            holder.iconList=(ImageView)convertView.findViewById(R.id.iconList);
            holder.titleList=(TextView)convertView.findViewById(R.id.title);
            holder.deskripsi=(TextView)convertView.findViewById(R.id.deskripsi);
            holder.background=(LinearLayout)convertView.findViewById(R.id.container);
        }else{
            holder=(ViewHolder) convertView.getTag();
        }

        //set value
        holder.iconList.setImageResource(ICON_MOBIL[position]);
        holder.titleList.setText(MERK_MOBIL[position]);
        holder.deskripsi.setText(DESKRIPSI_MOBIL[position]);


        //handle specific action
        holder.iconList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Logo "+MERK_MOBIL[position],Toast.LENGTH_LONG).show();
            }
        });

        holder.titleList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Merek "+MERK_MOBIL[position],Toast.LENGTH_LONG).show();
            }
        });

        //beda warna background
        if(position%2==0){
            holder.background.setBackgroundColor(R.color.greenYellow);
        }else{
            holder.background.setBackgroundColor(R.color.black);
        }

        convertView.setTag(holder);
        return convertView;
    }

    class ViewHolder{
        ImageView iconList;
        TextView titleList,deskripsi;
        LinearLayout background;
    }
}
