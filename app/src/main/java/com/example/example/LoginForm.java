package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.SearchEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.example.utilities.Konstanta;
import com.example.example.utilities.SessionManager;

public class LoginForm extends AppCompatActivity {
    private Context context = this;
    private EditText username,password;
    private TextView newUser;
    private Button buttonLogin;
    private CheckBox remember;
    private Toast toast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_form);

        //create view
        buttonLogin = findViewById(R.id.button_login);
        username = findViewById(R.id.inputUserName);
        password = findViewById(R.id.inputPassword);
        remember = findViewById(R.id.remember_me);
        newUser = findViewById(R.id.register_new_user);
        cekRemember();
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLogin();
            }
        });
        newUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pindahkeActivityRegister();
            }
        });
    }
    void checkLogin(){
        String uname = username.getText().toString().trim();
        String pass = password.getText().toString().trim();
        Boolean isremember = remember.isChecked();

        if (uname.equals("")) {
            Toast.makeText(context, getResources().getText(R.string.warn_uname_empty), Toast.LENGTH_SHORT).show();
        }
        else if (pass.equals("")){
            Toast.makeText(context, getResources().getText(R.string.warn_pass_empty), Toast.LENGTH_SHORT).show();
        }
        else {
            //sukses
            //simpan data2 login
            SessionManager.simpanDataLogin(context,uname,pass,isremember);

            pindahkeMainActivity("Welcome : "+uname);
        }
    }

    void cekRemember(){
        if(SessionManager.cekRemember(context)){
            String uname = SessionManager.getUnameSessionManager(context);
            String pass = SessionManager.getPassSessionManager(context);
            boolean isRemember = remember.isChecked();
            username.setText(uname);
            password.setText(pass);
            remember.setChecked(true);
        }
    }

    void pindahkeActivityRegister(){
        Intent intent = new Intent(context,Register.class);
        startActivity(intent);
    }

    void pindahkeMainActivity(String uname){
        Intent intent = new Intent(context,MainActivity.class);
        intent.putExtra(Konstanta.USERNAME,uname);
        startActivity(intent);
        finish();
    }

}
